const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const path = require('path');

module.exports =  {
    devtool: "source-map",
    plugins: [
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: { baseDir: ['./dist', './configs'] }
        })
    ],
    entry: {
        jsrunner: "./src/index.ts"
    },
    output: {
        path: path.resolve(__dirname, 'configs'),
        filename: '[name].js',
        library: "JSRunner",
        libraryTarget: 'var'
    },
    module: {
        loaders: [
            {
                test: /\.ts(x?)$/,
                loader: "babel-loader?presets[]=es2015!ts-loader",
                exclude: "/node_modules/",
            },
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    node: {
        fs: 'empty',
    }
};
