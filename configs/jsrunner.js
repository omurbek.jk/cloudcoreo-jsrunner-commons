module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var CONSTANTS = {
    SEVERITY_COLORS: {
        HIGH: '#E53E2B',
        MEDIUM: '#E49530',
        LOW: '#6b6b6b',
        ANY: '#6b6b6b',
        COUNT: '#ffffff'
    },
    SCHEME_FOR_TABLE: {
        DEFAULT: {
            'AWS Object ID': '__OBJECT__',
            'AWS Region': '+__OBJECT__.violations.__RULE__.region+',
            'AWS Tags': '__TAGS__'
        }
    },
    IS_VISIBLE_SEVERITY_LEVEL: {
        INTERNAL: false
    },
    WIDTH_FOR_ONE_COLUMN: 100,
    COLORS: {
        WHITE: '#ffffff',
        GREY: '#6b6b6b'
    },
    SEVERITY_LEVELS: {
        INFORMATIONAL: 'informational'
    },
    TEXT: {
        RULE: 'rule',
        RULES: 'rules'
    }
};
exports.default = CONSTANTS;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _emailType = __webpack_require__(11);

var _emailType2 = _interopRequireDefault(_emailType);

var _cloudObjectType = __webpack_require__(10);

var _cloudObjectType2 = _interopRequireDefault(_cloudObjectType);

var _scheme = __webpack_require__(12);

var _scheme2 = _interopRequireDefault(_scheme);

var _constants = __webpack_require__(0);

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CloudObject = function () {
    function CloudObject(cloudObjectArg, compositeReport, userProperties) {
        this.cloudObjectArg = cloudObjectArg;
        this.region = cloudObjectArg.region;
        this.cloudObjId = cloudObjectArg.cloudObjId;
        this.ruleId = cloudObjectArg.ruleId;
        this.compositeReport = compositeReport;
        this.userSuppression = compositeReport.userSuppression;
        this.userSchemes = compositeReport.userSchemes;
        this.compositeProp = userProperties;
        this.objByReport = this.compositeReport.violations[this.region][this.cloudObjId].violations[this.ruleId];
        this.objByReport.resource_id = this.ruleId;
        this.setCloudObjType();
        this.setEmailType();
        this.setScheme();
    }
    CloudObject.prototype.setCloudObjType = function () {
        var argsForCloudObjType = {
            userSuppression: this.userSuppression,
            userProperties: this.compositeProp,
            cloudObjId: this.cloudObjId, ruleId: this.ruleId
        };
        this.cloudObjType = new _cloudObjectType2.default(argsForCloudObjType);
    };
    CloudObject.prototype.setEmailType = function () {
        var argsForEmailType = {
            violations: this.compositeReport.violations[this.region][this.cloudObjId],
            tags: this.compositeReport.violations[this.region][this.cloudObjId].tags,
            compositeProp: this.compositeProp,
            cloudObjId: this.cloudObjId, ruleId: this.ruleId
        };
        this.emailType = new _emailType2.default(argsForEmailType);
    };
    CloudObject.prototype.setScheme = function () {
        var argsForScheme = {
            userSchemes: this.userSchemes,
            violations: this.compositeReport.violations[this.region],
            tags: this.compositeReport.violations[this.region][this.cloudObjId].tags,
            compositeProp: this.compositeProp,
            cloudObjId: this.cloudObjId, ruleId: this.ruleId,
            cloudObjType: this.cloudObjType, emailType: this.emailType
        };
        this.scheme = new _scheme2.default(argsForScheme);
    };
    CloudObject.prototype.getType = function () {
        return this.cloudObjType.getType();
    };
    CloudObject.prototype.getNotificationEmail = function () {
        return this.emailType.getNotificationEmail();
    };
    CloudObject.prototype.getSeverityLevel = function () {
        return this.objByReport.level.toLowerCase() || '';
    };
    CloudObject.prototype.addSuppressUntilSloganForColumn = function () {
        this.scheme.schemeKeys.push('Suppression Until');
    };
    CloudObject.prototype.pushSuppressDateToSchemeValues = function () {
        var schemeValue = this.cloudObjType.getTransformDate() || '';
        var hrefForValue = '';
        this.scheme.schemeValues.push({ schemeValue: schemeValue, hrefForValue: hrefForValue });
    };
    CloudObject.prototype.getColor = function () {
        var levels = { high: _constants2.default.SEVERITY_COLORS.HIGH, medium: _constants2.default.SEVERITY_COLORS.MEDIUM, low: _constants2.default.SEVERITY_COLORS.LOW };
        var countColor = _constants2.default.SEVERITY_COLORS.COUNT;
        var level = this.objByReport.level.toLowerCase();
        if (!levels[level]) return countColor;
        return levels[level];
    };
    CloudObject.prototype.isShownDate = function () {
        return this.cloudObjType.isShownDate();
    };
    CloudObject.prototype.getDisplayName = function () {
        return this.objByReport.display_name;
    };
    CloudObject.prototype.getKBLink = function () {
        return this.objByReport.link;
    };
    CloudObject.prototype.getDescription = function () {
        return this.objByReport.description;
    };
    CloudObject.prototype.getSuppressionData = function () {
        return this.cloudObjType.getUserSuppressionDate();
    };
    CloudObject.prototype.getTableKeys = function () {
        return this.scheme.getSchemeKeys();
    };
    CloudObject.prototype.getValueForTable = function () {
        return this.scheme.getValues();
    };
    CloudObject.prototype.getResourceName = function () {
        return this.objByReport.resource_id;
    };
    return CloudObject;
}();
exports.default = CloudObject;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cards = __webpack_require__(9);

var _cards2 = _interopRequireDefault(_cards);

var _cloudObject = __webpack_require__(1);

var _cloudObject2 = _interopRequireDefault(_cloudObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CardBuilder = function () {
    function CardBuilder() {}
    /**
     * Build cards
     * @param compositeReport
     * @param userProperties
     * @returns {Cards}
     */
    CardBuilder.build = function (compositeReport, userProperties) {
        var cards = new _cards2.default();
        Object.keys(compositeReport.violations).forEach(function (region) {
            Object.keys(compositeReport.violations[region]).forEach(function (cloudObjId) {
                var violationWay = compositeReport.violations[region][cloudObjId];
                Object.keys(violationWay.violations).forEach(function (ruleId) {
                    var cloudObjectArg = { region: region, cloudObjId: cloudObjId, ruleId: ruleId };
                    cards.add(new _cloudObject2.default(cloudObjectArg, compositeReport, userProperties));
                });
            });
        });
        return cards;
    };
    return CardBuilder;
}();
exports.default = CardBuilder;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cloudObject = __webpack_require__(1);

var _cloudObject2 = _interopRequireDefault(_cloudObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SuppressionJSON = function () {
    function SuppressionJSON() {}
    SuppressionJSON.build = function (compositeReport, userProperties) {
        Object.keys(compositeReport.violations).forEach(function (region) {
            Object.keys(compositeReport.violations[region]).forEach(function (cloudObjId) {
                var violationWay = compositeReport.violations[region][cloudObjId].violations;
                Object.keys(violationWay).forEach(function (ruleId) {
                    var cloudObjectArg = { region: region, cloudObjId: cloudObjId, ruleId: ruleId };
                    var cloudObject = new _cloudObject2.default(cloudObjectArg, compositeReport, userProperties);
                    var suppressionType = cloudObject.getType();
                    var suppressionData = cloudObject.getSuppressionData();
                    if (suppressionData) {
                        violationWay[ruleId].suppression_until = suppressionData;
                        if (suppressionType === 'suppressed') {
                            violationWay[ruleId].suppressed = 'true';
                        }
                    }
                });
            });
        });
        return compositeReport;
    };
    return SuppressionJSON;
}();
exports.default = SuppressionJSON;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var Email = function () {
    function Email(argsForEmail) {
        this.type = 'email';
        this.send_on = argsForEmail.compositeProp.SEND_ON.toString();
        this.allow_empty = argsForEmail.compositeProp.ALLOW_EMPTY.toString();
        this.payload_type = 'html';
        this.payload_type = 'html';
        this.endpoint = {
            to: argsForEmail.notificationEmail,
            subject: "[" + argsForEmail.compositeReport.compositeName + "] New Owner Tag Report for " + argsForEmail.compositeReport.planName + " plan from CloudCoreo"
        };
        this.num_violations = argsForEmail.cards.getLengthCards().toString();
        this.num_instances = argsForEmail.cards.getLengthCards().toString();
        this.numberOfViolatingCloudObjects = argsForEmail.cards.getNumberOfViolatingCloudObjects(argsForEmail.compositeReport);
        this.payload = argsForEmail.cards.getHTMLHeader(argsForEmail.compositeReport) + argsForEmail.cards.getHTMLBody() + argsForEmail.cards.getHTMLNoViolations(argsForEmail.compositeReport);
    }
    return Email;
}();
exports.default = Email;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(0);

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CardHTML = function () {
    function CardHTML() {
        this.card = {
            violations: [],
            suppressed: []
        };
    }
    CardHTML.prototype.createTextColor = function (color) {
        return color === _constants2.default.COLORS.WHITE ? _constants2.default.COLORS.GREY : _constants2.default.COLORS.WHITE;
    };
    CardHTML.prototype.getHTMLSuppressedCounterForCard = function () {
        var suppressLength = this.card.suppressed.length;
        if (!suppressLength) return '';
        return "<span style=\"background: #d7d7d7; color:#6b6b6b;font-size: 11px;\n                        display: inline-block;padding: 5px; border-radius:3px;\"\n                        >" + suppressLength + " SUPPRESSED</span>";
    };
    CardHTML.prototype.getHTMLForSeverityLevel = function (color, textColor) {
        var severityLevel = this.firstCloudObj.getSeverityLevel();
        if (!severityLevel) return '';
        return "<div style=\"padding: 5px;margin-bottom: 10px;font-size: 14px;\n                border:1px solid #e4e4e4; text-transform:capitalize;\n                display:inline-block;background:" + color + ";font-weight: bold;color:" + textColor + ";\">\n                " + severityLevel + " Severity</div>";
    };
    CardHTML.prototype.getHTMLForKBLink = function () {
        var kbLink = this.firstCloudObj.getKBLink();
        if (!kbLink) return '';
        return "<span style=\"float: right;\">\n                    <a style=\"font-size: 14px;color:#2B7AE5;text-decoration:none;\" \n                    href=\"" + kbLink + '"> MORE INFO \u203A</a></span>';
    };
    CardHTML.prototype.getHTMLForDisplayName = function () {
        var displayName = this.firstCloudObj.getDisplayName();
        if (!displayName) return '';
        return "<span>" + displayName + "</span>";
    };
    CardHTML.prototype.getHTMLForResourceId = function () {
        var resourceId = this.firstCloudObj.getResourceName();
        if (!resourceId) return '';
        return "<span style=\"font-family: Arial; font-size: 12px; \n                color:#000\">" + resourceId + "</span>";
    };
    CardHTML.prototype.getHTMLCountForCloudObjects = function () {
        var cloudObjectLength = this.card.violations.length;
        if (cloudObjectLength < 0) return '';
        return "<p style=\"margin: 5px 0 5px;font-size: 14px;color:#000;\"\n                >" + cloudObjectLength + " matching Cloud Objects</p>";
    };
    CardHTML.prototype.getHTMLForDescription = function () {
        var description = this.firstCloudObj.getDescription();
        if (!description) return '';
        return "<p style=\"margin:0 0 10px;font-size: 14px;color:#000;\">" + this.firstCloudObj.getDescription() + "</p>";
    };
    CardHTML.prototype.getHTMLHeaderForCard = function () {
        this.firstCloudObj = this.card.violations[0];
        if (!this.firstCloudObj) return '';
        var color = this.firstCloudObj.getColor();
        var htmlForSeverityLevel = this.getHTMLForSeverityLevel(color, this.createTextColor(color));
        var htmlForDisplayName = this.getHTMLForDisplayName();
        var htmlForKbLink = this.getHTMLForKBLink();
        var htmlForResourceId = this.getHTMLForResourceId();
        var htmlCountForCloudObjects = this.getHTMLCountForCloudObjects();
        var htmlSuppressedCounterForCard = this.getHTMLSuppressedCounterForCard();
        var htmlForDescription = this.getHTMLForDescription();
        return "\n            <div style=\"font-size: 16px; padding: 0 10px 10px;\">\n                <div>\n                    " + htmlForSeverityLevel + "\n                </div>\n                <div style=\"font-size: 18px;color:#000;font-weight: bold;\">\n                    " + htmlForDisplayName + "\n                    " + htmlForKbLink + "\n                </div>\n                " + htmlForResourceId + "\n                " + htmlCountForCloudObjects + "\n                " + htmlSuppressedCounterForCard + "\n                <div style=\"height:1px;width:100%; background:#e4e4e4;margin:10px 0 15px;\"></div>\n                " + htmlForDescription + "\n            </div>\n        ";
    };
    CardHTML.prototype.getColumnWidth = function (tableKeys) {
        var WIDTH_FOR_ONE_COLUMN = _constants2.default.WIDTH_FOR_ONE_COLUMN;
        return tableKeys.length < 1 ? WIDTH_FOR_ONE_COLUMN : WIDTH_FOR_ONE_COLUMN / tableKeys.length;
    };
    CardHTML.prototype.getHTMLTableKeys = function () {
        var _this = this;
        if (!this.firstCloudObj) return '';
        var tableKeys = this.firstCloudObj.getTableKeys();
        if (!tableKeys) return '';
        var columnWidth = this.getColumnWidth(tableKeys);
        var wrapHTML = '<div style="display:flex;flex-wrap:wrap;">';
        return tableKeys.reduce(function (html, key) {
            return html + _this.getHTMLTableKey(columnWidth, key);
        }, wrapHTML) + '</div>';
    };
    CardHTML.prototype.getHTMLTableKey = function (columnWidth, key) {
        return "<div style=\"width:calc(" + columnWidth + "% - 20px);padding: 10px 10px 5px;\nfont-size: 14px;color:#000;font-weight: bold;\">\n                " + key + "\n            </div>";
    };
    CardHTML.prototype.getHTMLBodyForCard = function () {
        var _this = this;
        var htmlBodyForCard = '<div style="background:#f2f2f2;">';
        htmlBodyForCard += this.getHTMLTableKeys();
        htmlBodyForCard += this.card.violations.reduce(function (html, cloudObj) {
            return html + _this.getHTMLValueFor(cloudObj.getValueForTable());
        }, '');
        if (this.card.violations.length && this.card.suppressed.length) {
            htmlBodyForCard += "<div style=\"background: #d7d7d7;padding-top: 10px;\">\n                <span style=\"margin-left:10px;color:#6b6b6b;font-size: 11px;\">SUPPRESSED</span>";
            this.card.suppressed.forEach(function (cloudObj) {
                htmlBodyForCard += _this.getHTMLValueFor(cloudObj.getValueForTable());
            });
            htmlBodyForCard += '</div>';
        }
        return htmlBodyForCard + '</div>';
    };
    CardHTML.prototype.getHTMLValueFor = function (schemeForTable) {
        var schemes = schemeForTable;
        var htmlValueForTable = '<div style="display:flex; flex-wrap:wrap; padding-bottom:5px;">';
        var columnWidth = this.getColumnWidth(schemes);
        schemes.forEach(function (value) {
            htmlValueForTable += "<div style=\"word-break:break-all;font-size:14px; color:#000; \n                width:calc(" + columnWidth + "% - 20px); padding:5px 10px;\">";
            htmlValueForTable += value.hrefForValue ? "<a href=\"" + value.hrefForValue + "\">" + value.schemeValue + "</a>" : value.schemeValue;
            htmlValueForTable += "</div>";
        });
        return htmlValueForTable + "</div>";
    };
    CardHTML.prototype.getHTMLForCard = function () {
        var htmlForCard = '<div style="border:1px solid #e4e4e4;margin-bottom:20px;">';
        htmlForCard += this.getHTMLHeaderForCard();
        htmlForCard += this.getHTMLBodyForCard();
        return htmlForCard + '</div>';
    };
    CardHTML.prototype.getHTMLForNoViolations = function () {
        if (this.card.violations.length !== 0) return '';
        var suppressCloudObj = this.card.suppressed[0];
        if (typeof suppressCloudObj === 'undefined') return '';
        var displayName = suppressCloudObj.getDisplayName();
        var kbLink = suppressCloudObj.getKBLink();
        var htmlSuppressedCounterForCard = this.getHTMLSuppressedCounterForCard();
        return "\n            <div style=\"border:1px solid #d6d6d6; border-left:0; border-right:0;padding: 10px 0;\n                display: flex;flex-wrap: wrap;align-items: center;\n            margin-left: 10px;font-family: Arial, sans-serif;font-weight: bold;\">\n                <span style=\"font-size: 18px;color: #000; margin-right: 10px;\">\n                    " + displayName + "  \n                </span>\n                " + htmlSuppressedCounterForCard + "\n                <span style=\"margin-left:auto;\">\n                    <a style=\"font-size: 14px;color:#2B7AE5;text-decoration:none;\"\n                        href=\"" + kbLink + '"> MORE INFO \u203A</a>\n                </span>\n            </div>';
    };
    return CardHTML;
}();
exports.default = CardHTML;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _severityLevels = __webpack_require__(13);

var _severityLevels2 = _interopRequireDefault(_severityLevels);

var _constants = __webpack_require__(0);

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CardsHTML = function () {
    function CardsHTML() {
        this.cards = new Map();
    }
    CardsHTML.prototype.createSeverityLevel = function () {
        var severityLevels = new _severityLevels2.default();
        this.cards.forEach(function (card) {
            var severityLevel = card.getSeverityLevel();
            var violationLength = card.getLengthViolations();
            if (severityLevels.has(severityLevel)) {
                severityLevels.increaseCountBy(severityLevel, violationLength);
            }
        });
        return severityLevels;
    };
    CardsHTML.prototype.getLengthAlertLists = function (alertList) {
        return alertList ? alertList.length : this.cards.size;
    };
    CardsHTML.prototype.getHTMLLogoWithAccount = function () {
        return "\n            <div style=\"background: #fff;font-family:'Arial', sans-serif;\">\n                <div style=\"font-family:'Arial', sans-serif;text-align:center;background: #f2f2f2; padding: 15px 0 14px\">\n                    <a target=\"_blank\" href=\"https://www.cloudcoreo.com/\"><img src=\"https://appassets.cloudcoreo.com/img/logo/logo-alt-black.png\" alt=\"\"></a>\n                </div>\n                <!-- ACCOUNT_SUGGESTION_START -->\n                <div style=\"font-family:'Arial', sans-serif;text-align:center; background:#e4e4e4; padding: 14px; font-size:16px;\">\n                    Don't have an account? <a href=\"https://www.cloudcoreo.com/early-access/\">Sign up for early access.</a>\n                </div>\n                <!--ACCOUNT_SUGGESTION_END-->\n            </div>\n        ";
    };
    CardsHTML.prototype.getHTMLAuditSummarySlogan = function (compositeReport) {
        var compositeAndPlan = '';
        var cloudAccount = '';
        var teamName = '';
        if (compositeReport.teamName) {
            teamName = "<p style=\"margin:0\">Cloud Team: " + compositeReport.teamName + "</p>";
        }
        if (compositeReport.compositeName && compositeReport.planName) {
            compositeAndPlan = "<p style=\"margin:0\">" + compositeReport.compositeName + ": " + compositeReport.planName + "</p>";
        }
        if (compositeReport.cloudAccount) {
            cloudAccount = "<p style=\"margin:0\">Cloud account: " + compositeReport.cloudAccount + "</p>";
        }
        return "<div style=\"text-align:center;padding-top:30px;padding-bottom:15px;font-size:18px;color:#000;\">\n                    <p style=\"font-size:24px;margin:0;color:#000; font-weight:bold;\">Audit Summary</p>\n                    " + compositeAndPlan + "\n                    " + cloudAccount + "\n                    " + teamName + "\n                </div>";
    };
    CardsHTML.prototype.generateHTMLForSeverityCounter = function (severityLevel, level) {
        var levels = {
            high: _constants2.default.SEVERITY_COLORS.HIGH,
            medium: _constants2.default.SEVERITY_COLORS.MEDIUM,
            low: _constants2.default.SEVERITY_COLORS.LOW
        };
        var color = _constants2.default.SEVERITY_COLORS.ANY;
        if (levels.hasOwnProperty(level)) {
            color = levels[level];
        }
        return "<div style=\"width:calc(100% / 3); height:90px;padding-top:10px;color:white;\">\n                    <div style=\"font-size:42px;color:" + color + ";\n                    margin-bottom: 6px;\">" + severityLevel[level] + " </div>\n                    <div style=\"padding:10px;background:" + color + ";text-transform: capitalize;\n                    \">" + level + " Severity</div>\n                </div>";
    };
    CardsHTML.prototype.getHTMLAuditSummaryBody = function (severityLevel) {
        return "<div style=\"color:#000;font-size: 14px;flex-wrap:wrap;text-align: center;\n                    font-weight: bold; padding:0 10px 10px;\">\n                    <div style=\"width:calc(100% - 40px);background:#fff;height:90px;padding: 10px 20px 0;\">\n                        <div style=\"font-size:42px;\">" + severityLevel.sum + "</div>\n                        Violating <br> Cloud Objects\n                    </div>\n                    <div style=\"display:flex;\">\n                        " + this.generateHTMLForSeverityCounter(severityLevel, 'high') + "\n                        " + this.generateHTMLForSeverityCounter(severityLevel, 'medium') + "\n                        " + this.generateHTMLForSeverityCounter(severityLevel, 'low') + "\n                    </div>\n                </div>";
    };
    CardsHTML.prototype.getHTMLAuditSummary = function (compositeReport) {
        var severityLevel = this.createSeverityLevel();
        return "<div style=\"background:#f2f2f2;font-family:'Arial', 'sans-serif';margin-top:10px;\">\n                " + this.getHTMLAuditSummarySlogan(compositeReport) + "\n                " + this.getHTMLAuditSummaryBody(severityLevel) + "\n            </div>";
    };
    CardsHTML.prototype.getNumberOfViolatingCloudObjects = function (compositeReport) {
        var severityLevel = this.createSeverityLevel();
        return severityLevel.sum;
    };
    CardsHTML.prototype.getHTMLAuditDetails = function (compositeReport) {
        var getTextRuleBy = function getTextRuleBy(length) {
            return length === 1 ? _constants2.default.TEXT.RULE : _constants2.default.TEXT.RULES;
        };
        var alertListLength = this.getLengthAlertLists(compositeReport.alertList);
        var cardsCountForViolationFound = this.cards.size;
        var textForAlertList = getTextRuleBy(alertListLength);
        var textForCardsCount = getTextRuleBy(cardsCountForViolationFound);
        return "\n            <div style=\"text-align:center; color:#000;font-size: 14px;padding: 10px 0;\n            font-family: 'Arial', sans-serif;\">\n                <p style=\"font-weight: bold;font-size: 18px;margin: 0 3px;\">Audit Details</p>\n                <p style=\"font-style: italic;margin: 0 3px;\">Cloud Objects List (by Rule)</p>\n                <p style=\"margin: 0 3px;\">" + alertListLength + " " + textForAlertList + " run, " + cardsCountForViolationFound + " " + textForCardsCount + " found matching Cloud Objects\n                </p>\n            </div>\n        ";
    };
    CardsHTML.prototype.getHTMLHeaderForNoViolations = function () {
        return "\n            <div style=\"margin-left:10px;\">\n                <span style=\"font-family: Arial, sans-serif;padding: 5px;margin-bottom: 10px;font-size: 14px;\n                    border:1px solid #e4e4e4; text-transform:capitalize;\n                    display:inline-block;background:#f2f2f2;font-weight: bold;color:#6b6b6b;\">No Violations</span>\n            </div>\n        ";
    };
    CardsHTML.prototype.getHTMLForDisabledViolations = function (disableViol) {
        var HTML_FOR_DISABLED_VIOLATIONS = '';
        var link = '';
        var displayName = '';
        if (disableViol.display_name) {
            displayName = "<span style=\"font-size: 18px;color: #000; margin-right: 10px;\">\n                    " + disableViol.display_name.value + "\n                </span>";
        }
        if (disableViol.link) {
            link = "<span style=\"margin-left:auto;\"><a style=\"font-size: 14px;color:#2B7AE5;text-decoration:none;\"\n                        href=\"" + disableViol.link.value + '"> MORE INFO \u203A</a></span>';
        }
        HTML_FOR_DISABLED_VIOLATIONS += "\n            <div style=\"border:1px solid #d6d6d6; border-left:0; border-right:0;padding: 10px 0;\n                display: flex;flex-wrap: wrap;align-items: center;\n            margin-left: 10px;font-family: Arial, sans-serif;font-weight: bold;\">\n                   " + displayName + "\n                   " + link + "\n            </div>";
        return HTML_FOR_DISABLED_VIOLATIONS;
    };
    CardsHTML.prototype.getHTMLHeader = function (compositeReport) {
        var HTML_HEADER = this.getHTMLLogoWithAccount();
        HTML_HEADER += this.getHTMLAuditSummary(compositeReport);
        HTML_HEADER += this.getHTMLAuditDetails(compositeReport);
        return HTML_HEADER;
    };
    CardsHTML.prototype.getHTMLBody = function () {
        var HTML_BODY = '';
        this.cards.forEach(function (card) {
            if (_constants2.default.IS_VISIBLE_SEVERITY_LEVEL[card.getSeverityLevel().toUpperCase()] === false) {
                return;
            }
            HTML_BODY += card.getHTMLForCard();
        });
        return HTML_BODY;
    };
    CardsHTML.prototype.getHTMLNoViolations = function (compositeReport) {
        var _this = this;
        var HTML_NO_VIOLATIONS = '';
        var HTML_FOR_NO_VIOLATIONS = '';
        var disabled = compositeReport.disabled;
        this.cards.forEach(function (card) {
            return HTML_FOR_NO_VIOLATIONS += card.getHTMLForNoViolations();
        });
        var alertLists = new Set(compositeReport.alertList);
        Object.keys(disabled).forEach(function (rule) {
            var disableViol = disabled[rule];
            if (!alertLists.has(rule)) {
                HTML_FOR_NO_VIOLATIONS += _this.getHTMLForDisabledViolations(disableViol);
            }
        });
        if (HTML_FOR_NO_VIOLATIONS) HTML_NO_VIOLATIONS += this.getHTMLHeaderForNoViolations();
        HTML_NO_VIOLATIONS += HTML_FOR_NO_VIOLATIONS;
        return HTML_NO_VIOLATIONS;
    };
    return CardsHTML;
}();
exports.default = CardsHTML;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.CloudCoreoJSRunner = undefined;

var _card = __webpack_require__(2);

var _card2 = _interopRequireDefault(_card);

var _email = __webpack_require__(4);

var _email2 = _interopRequireDefault(_email);

var _suppressionJson = __webpack_require__(3);

var _suppressionJson2 = _interopRequireDefault(_suppressionJson);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CloudCoreoJSRunner = function () {
    function CloudCoreoJSRunner() {}
    CloudCoreoJSRunner.createEmails = function (compositeReport, compositeProp) {
        var cards = _card2.default.build(compositeReport, compositeProp);
        var noOwnerEmail = compositeProp['NO_OWNER_EMAIL'];
        cards.sortCardsBySeverityLevel();
        var sortedCardsByEmail = cards.getSortedCardsByEmail();
        var hasEmails = !!sortedCardsByEmail.size;
        var argsForEmail = [];
        if (hasEmails) {
            sortedCardsByEmail.forEach(function (cardsSortedByEmail, notificationEmail) {
                cardsSortedByEmail.setTableKeysForCards();
                argsForEmail.push({ cards: cardsSortedByEmail, notificationEmail: notificationEmail, compositeReport: compositeReport, compositeProp: compositeProp });
            });
        } else {
            argsForEmail.push({ cards: cards, notificationEmail: noOwnerEmail, compositeReport: compositeReport, compositeProp: compositeProp });
        }
        if (compositeProp.OWNER_TAG !== 'NOT_A_TAG' && noOwnerEmail.length > 0) {
            argsForEmail.push({ cards: cards, notificationEmail: noOwnerEmail, compositeReport: compositeReport, compositeProp: compositeProp });
        }
        var emails = argsForEmail.map(function (arg) {
            return new _email2.default(arg);
        });
        emails = emails.filter(function (email) {
            if (email.allow_empty === 'true') {
                return email;
            } else if (parseInt(email.num_violations, 10) > 0) {
                return email;
            }
        });
        var createSubject = function createSubject(compositeReport) {
            if (compositeReport.htmlReportSubject) return "" + compositeReport.htmlReportSubject;else return "[" + compositeReport.compositeName + "] New Detailed Report for " + compositeReport.planName + " plan from CloudCoreo";
        };
        var foundEmail = emails.find(function (email) {
            return email.endpoint.to === noOwnerEmail;
        });
        if (foundEmail) {
            foundEmail.endpoint.subject = createSubject(compositeReport);
        }
        return emails;
    };
    /**
     * Get the JSONWithSuppress
     * @param compositeReport
     * @param compositeProp
     */
    CloudCoreoJSRunner.createJSONWithSuppress = function (compositeReport, compositeProp) {
        return _suppressionJson2.default.build(compositeReport, compositeProp);
    };
    return CloudCoreoJSRunner;
}();
exports.CloudCoreoJSRunner = CloudCoreoJSRunner;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cardHtml = __webpack_require__(5);

var _cardHtml2 = _interopRequireDefault(_cardHtml);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var __extends = undefined && undefined.__extends || function () {
    var extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function (d, b) {
        d.__proto__ = b;
    } || function (d, b) {
        for (var p in b) {
            if (b.hasOwnProperty(p)) d[p] = b[p];
        }
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
}();

var Card = function (_super) {
    __extends(Card, _super);
    function Card() {
        var _this = _super.call(this) || this;
        _this.card = {
            violations: [],
            suppressed: []
        };
        return _this;
    }
    Card.prototype.getViolationLength = function () {
        return this.card.violations.length;
    };
    /**
     * Push CloudObject to Card by CloudObjectType
     * @param cloudObject
     */
    Card.prototype.add = function (cloudObject) {
        this.card[cloudObject.getType()].push(cloudObject);
    };
    Card.prototype.hasSuppressDate = function () {
        var hasDateInViolations = this.card.violations.some(function (cloudObj) {
            return cloudObj.isShownDate();
        });
        var hasDateInSuppression = this.card.suppressed.some(function (cloudObj) {
            return cloudObj.isShownDate();
        });
        return hasDateInViolations || hasDateInSuppression;
    };
    Card.prototype.setSuppressUntilKeyForTableHeaderBy = function (key) {
        this.card[key].forEach(function (cloudObj) {
            cloudObj.addSuppressUntilSloganForColumn();
            cloudObj.pushSuppressDateToSchemeValues();
        });
    };
    Card.prototype.setSuppressUntilKeyForTableHeader = function () {
        this.setSuppressUntilKeyForTableHeaderBy('violations');
        this.setSuppressUntilKeyForTableHeaderBy('suppressed');
    };
    Card.prototype.getRuleId = function () {
        return this.card.violations.length > 0 ? this.card.violations[0].ruleId : this.card.suppressed[0].ruleId;
    };
    Card.prototype.getSortedCardByEmail = function () {
        var sortedCard = new Map();
        this.applyCloudObject(sortedCard, 'violations');
        this.applyCloudObject(sortedCard, 'suppressed');
        return sortedCard;
    };
    Card.prototype.getLengthViolations = function () {
        return this.card.violations.length;
    };
    Card.prototype.getSeverityLevel = function () {
        var firstObj = this.card.violations[0];
        if (!firstObj) return '';
        return firstObj.getSeverityLevel();
    };
    Card.prototype.applyCloudObject = function (sortedCard, cloudObjectType) {
        this.card[cloudObjectType].forEach(function (cloudObject) {
            var notificationEmail = cloudObject.getNotificationEmail();
            if (!sortedCard.has(notificationEmail)) sortedCard.set(notificationEmail, new Card());
            sortedCard.get(notificationEmail).add(cloudObject);
        });
    };
    return Card;
}(_cardHtml2.default);
exports.default = Card;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _card = __webpack_require__(8);

var _card2 = _interopRequireDefault(_card);

var _cardsHtml = __webpack_require__(6);

var _cardsHtml2 = _interopRequireDefault(_cardsHtml);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var __extends = undefined && undefined.__extends || function () {
    var extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function (d, b) {
        d.__proto__ = b;
    } || function (d, b) {
        for (var p in b) {
            if (b.hasOwnProperty(p)) d[p] = b[p];
        }
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
}();

var Cards = function (_super) {
    __extends(Cards, _super);
    function Cards() {
        var _this = _super.call(this) || this;
        _this.cards = new Map();
        return _this;
    }
    Cards.prototype.setTableKeysForCards = function () {
        this.cards.forEach(function (card) {
            if (card.hasSuppressDate()) card.setSuppressUntilKeyForTableHeader();
        });
    };
    Cards.prototype.add = function (cloudObject) {
        if (this.has(cloudObject.ruleId)) {
            this.getCardBy(cloudObject.ruleId).add(cloudObject);
            return;
        }
        var card = new _card2.default();
        card.add(cloudObject);
        this.create(cloudObject.ruleId, card);
    };
    Cards.prototype.sortCardsBySeverityLevel = function () {
        var _this = this;
        var prioritySort = { high: [], medium: [], low: [], anything: [] };
        this.cards.forEach(function (card, rule) {
            var severityLevel = card.getSeverityLevel();
            if (prioritySort[severityLevel]) {
                prioritySort[severityLevel].push({ rule: rule, card: card });
                return;
            }
            prioritySort.anything.push({ rule: rule, card: card });
        });
        this.cards = new Map();
        Object.keys(prioritySort).forEach(function (priority) {
            prioritySort[priority].forEach(function (cardObj) {
                var rule = cardObj.rule,
                    card = cardObj.card;
                _this.create(rule, card);
            });
        });
    };
    Cards.prototype.getSortedCardsByEmail = function () {
        var sortedCardsByEmail = new Map();
        this.cards.forEach(function (card) {
            var cardSortedByEmail = card.getSortedCardByEmail();
            cardSortedByEmail.forEach(function (card, email) {
                if (sortedCardsByEmail.has(email)) {
                    sortedCardsByEmail.get(email).create(card.getRuleId(), cardSortedByEmail.get(email));
                    return;
                }
                var cards = new Cards();
                cards.create(card.getRuleId(), cardSortedByEmail.get(email));
                sortedCardsByEmail.set(email, cards);
            });
        });
        return sortedCardsByEmail;
    };
    Cards.prototype.has = function (rule) {
        return this.cards.has(rule);
    };
    Cards.prototype.create = function (rule, card) {
        this.cards.set(rule, card);
    };
    Cards.prototype.getCardBy = function (rule) {
        return this.cards.get(rule);
    };
    Cards.prototype.getLengthCards = function () {
        var counter = 0;
        this.cards.forEach(function (card) {
            return counter += card.getLengthViolations();
        });
        return counter;
    };
    return Cards;
}(_cardsHtml2.default);
exports.default = Cards;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _dateformat = __webpack_require__(14);

var dateformat = _interopRequireWildcard(_dateformat);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var CloudObjectType = function () {
    function CloudObjectType(propertiesForCloudObjType) {
        this.userSuppression = propertiesForCloudObjType.userSuppression;
        this.userProperties = propertiesForCloudObjType.compositeProp;
        this.ruleId = propertiesForCloudObjType.ruleId;
        this.cloudObjId = propertiesForCloudObjType.cloudObjId;
        this.setUserSuppressionDate();
        this.setCorrectSuppressDate();
        this.setCloudObjectType();
        this.setTransformDate();
    }
    CloudObjectType.prototype.setUserSuppressionDate = function () {
        var _this = this;
        if (!this.userSuppression) return;
        var userSuppression = this.userSuppression[this.ruleId];
        if (typeof userSuppression === 'undefined') return;
        var suppression = this.userSuppression[this.ruleId].find(function (suppression) {
            return suppression.hasOwnProperty(_this.cloudObjId);
        });
        if (suppression && suppression.hasOwnProperty(this.cloudObjId)) {
            this.userSuppressionDate = suppression[this.cloudObjId];
        }
    };
    CloudObjectType.prototype.setCorrectSuppressDate = function () {
        if (this.userSuppressionDate === '') return;
        this.correctSuppressDate = new Date(this.userSuppressionDate);
    };
    CloudObjectType.prototype.setCloudObjectType = function () {
        var nowDate = new Date();
        var isSuppressed = nowDate <= this.correctSuppressDate;
        this.type = isSuppressed || !this.correctSuppressDate ? 'suppressed' : 'violations';
    };
    CloudObjectType.prototype.setTransformDate = function () {
        if (!this.userSuppressionDate) return;
        var suppressionDate = new Date(this.userSuppressionDate);
        this.transformDate = dateformat.default ? dateformat.default(suppressionDate, 'dd/mm/yyyy hh:MM') : dateformat(suppressionDate, 'dd/mm/yyyy hh:MM');
    };
    CloudObjectType.prototype.getUserSuppressionDate = function () {
        return this.userSuppressionDate;
    };
    CloudObjectType.prototype.getCorrectSuppressDate = function () {
        return this.correctSuppressDate;
    };
    CloudObjectType.prototype.getType = function () {
        return this.type;
    };
    CloudObjectType.prototype.getTransformDate = function () {
        return this.transformDate;
    };
    CloudObjectType.prototype.isShownDate = function () {
        return typeof this.userSuppressionDate !== 'undefined';
    };
    return CloudObjectType;
}();
exports.default = CloudObjectType;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var EmailType = function () {
    function EmailType(emailProperties) {
        this.violations = emailProperties.violations;
        this.tags = emailProperties.tags;
        this.ruleId = emailProperties.ruleId;
        this.cloudObjId = emailProperties.cloudObjId;
        this.OWNER_TAG = emailProperties.compositeProp.OWNER_TAG;
        this.NO_OWNER_EMAIL = emailProperties.compositeProp.NO_OWNER_EMAIL;
        this.setEmailFromTag();
        this.setNotificationType();
        this.setNotificationEmail();
        this.setValueForTable();
    }
    EmailType.prototype.setEmailFromTag = function () {
        var _this = this;
        var tagsIsUndefined = typeof this.tags == 'undefined';
        var tagsIsNull = this.tags == null;
        if (tagsIsUndefined || tagsIsNull) return;
        this.tags.forEach(function (tag) {
            if (tag.hasOwnProperty('tag')) tag = tag['tag'];
            var hasEmail = tag.key === _this.OWNER_TAG;
            if (hasEmail) _this.emailFromTag = tag.value;
        });
        var tagsIsArray = this.tags && Array.isArray(this.tags) && this.tags.length > 0 && typeof this.emailFromTag === 'undefined';
        if (tagsIsArray) this.emailFromTag = '';
    };
    EmailType.prototype.setNotificationType = function () {
        var isDefaultOwnerTag = this.OWNER_TAG === 'NOT_A_TAG';
        if (isDefaultOwnerTag) {
            this.emailType = 'userEmail';
            return;
        }
        var hasEmail = typeof this.emailFromTag !== 'undefined';
        var isDifferentEmail = this.NO_OWNER_EMAIL !== this.emailFromTag;
        var isEmptyEmail = this.emailFromTag !== '';
        var isTagEmail = hasEmail && isDifferentEmail && isEmptyEmail;
        this.emailType = isTagEmail ? 'tagEmail' : 'noOwnerEmail';
    };
    EmailType.prototype.setNotificationEmail = function () {
        var isNoOwnerEmail = this.emailType === 'userEmail' || this.emailType === 'noOwnerEmail';
        this.notificationEmail = isNoOwnerEmail ? this.NO_OWNER_EMAIL : this.emailFromTag;
    };
    EmailType.prototype.setValueForTable = function () {
        var isNoOwner = this.emailType === 'noOwnerEmail';
        var isTagEmail = !isNoOwner && this.emailType === 'tagEmail';
        if (isNoOwner) {
            this.valueForTable = 'No Owner';
            return;
        }
        if (isTagEmail) this.valueForTable = this.emailFromTag;
    };
    EmailType.prototype.getNotificationEmail = function () {
        return this.notificationEmail;
    };
    EmailType.prototype.getValueForTable = function () {
        return this.valueForTable;
    };
    EmailType.prototype.isShownEmail = function () {
        return !!this.valueForTable;
    };
    return EmailType;
}();
exports.default = EmailType;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _constants = __webpack_require__(0);

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Scheme = function () {
    function Scheme(properties) {
        this.replaceComponentArrowFor = function (value) {
            return value.replace(/[<>]/g, '');
        };
        this.userSchemes = properties.userSchemes;
        this.violations = properties.violations;
        this.ruleId = properties.ruleId;
        this.cloudObjId = properties.cloudObjId;
        this.tags = properties.tags;
        this.cloudObjType = properties.cloudObjType;
        this.emailType = properties.emailType;
        this.setUserScheme();
        this.setSchemeKeys();
        this.setSchemeValues();
        this.setOwnerEmail();
    }
    Scheme.prototype.setUserScheme = function () {
        if (this.userSchemes && this.userSchemes[this.ruleId]) {
            this.userScheme = this.userSchemes[this.ruleId];
        } else if (this.userSchemes && this.userSchemes['default']) {
            this.userScheme = this.userSchemes['default'];
        } else {
            this.userScheme = _constants2.default.SCHEME_FOR_TABLE.DEFAULT;
        }
    };
    Scheme.prototype.setSchemeKeys = function () {
        if (!this.userScheme) return;
        this.schemeKeys = Object.keys(this.userScheme);
    };
    Scheme.prototype.setSchemeValues = function () {
        var _this = this;
        this.schemeValues = [];
        if (!(this.schemeKeys && this.schemeKeys.length)) return;
        this.schemeKeys.forEach(function (schemeKey) {
            var scheme = _this.replaceTagKeys(_this.userScheme[schemeKey], _this.tags);
            var replacedScheme = _this.replaceScheme(scheme, _this.violations);
            var hrefForValue = _this.createLinkBy(replacedScheme);
            var schemeValue = _this.replaceComponentArrowFor(_this.createSchemeValue(replacedScheme));
            _this.schemeValues.push({ schemeValue: schemeValue, hrefForValue: hrefForValue });
        });
    };
    Scheme.prototype.replaceTagKeys = function (valuePath, tags) {
        valuePath = this.replaceForValue(valuePath, '__TAGKEYS__', this.createTagsStr(tags));
        return this.replaceForValue(valuePath, '__TAGS__', this.createTagsForBuilder(tags));
    };
    Scheme.prototype.replaceForValue = function (valuePath, thatChange, toChangeThat) {
        if (valuePath.indexOf(thatChange) === -1) return valuePath;
        return valuePath.replace(thatChange, toChangeThat);
    };
    Scheme.prototype.replaceScheme = function (scheme, cloudObjects) {
        var _this = this;
        var regExpForSchemePart = /\+[a-zA-Z0-9\-\._\/]+\+/g;
        var schemeParts = scheme.match(regExpForSchemePart);
        if (schemeParts) {
            schemeParts.forEach(function (schemePart) {
                var valuePart = _this.getValueBy(schemePart, cloudObjects);
                scheme = scheme.replace(schemePart, valuePart);
            });
        }
        return scheme.replace(/__OBJECT__/g, this.cloudObjId).replace(/__RULE__/g, this.ruleId);
    };
    Scheme.prototype.createLinkBy = function (scheme) {
        var indexTilde = scheme.indexOf('~');
        if (indexTilde >= 0) return scheme.substring(indexTilde + 1, scheme.length);
        return '';
    };
    Scheme.prototype.createSchemeValue = function (scheme) {
        var indexForHref = scheme.indexOf('~');
        if (indexForHref !== -1) return scheme.substring(0, indexForHref);
        return scheme;
    };
    Scheme.prototype.getValueBy = function (schemePart, cloudObjects) {
        var _this = this;
        schemePart = schemePart.replace(/\+/g, '');
        schemePart.split('.').some(function (valuePath) {
            if (valuePath === '__OBJECT__') valuePath = _this.cloudObjId;
            if (valuePath === '__RULE__') valuePath = _this.ruleId;
            var hasValue = typeof cloudObjects === 'string';
            if (hasValue) return true;
            var hasCorrectPath = cloudObjects.hasOwnProperty(valuePath);
            if (hasCorrectPath) cloudObjects = cloudObjects[valuePath];
        });
        var hasCorrectValue = typeof cloudObjects === 'string' || typeof cloudObjects === 'number' || typeof cloudObjects === 'boolean';
        var value = hasCorrectValue ? cloudObjects.toString() : 'NONE';
        var isIAMLink = value.indexOf('arn:aws:iam') === -1 && value.indexOf('arn:aws') >= 0;
        if (isIAMLink) value = value.replace('/', '@');
        return value;
    };
    Scheme.prototype.setOwnerEmail = function () {
        if (!this.emailType.isShownEmail()) return;
        this.schemeKeys.push('Owner');
        var schemeValue = this.emailType.getValueForTable();
        var hrefForValue = "mailto:" + schemeValue;
        if (schemeValue === 'No Owner') hrefForValue = '';
        this.schemeValues.push({ schemeValue: schemeValue, hrefForValue: hrefForValue });
    };
    Scheme.prototype.createTagsStr = function (tagArray) {
        var hasValueInTag = typeof tagArray === 'undefined' || Array.isArray(tagArray) && tagArray.length === 0;
        if (hasValueInTag) return 'NONE';
        var createTag = function createTag(tagElem) {
            return tagElem.tag ? tagElem['tag']['key'] + ", " : tagElem['key'] + ", ";
        };
        return tagArray.reduce(function (tagsToString, tagElem) {
            return tagsToString + createTag(tagElem);
        }, '').replace(/, $/, '') || 'NONE';
    };
    Scheme.prototype.createTagsForBuilder = function (tagArray) {
        var hasValueInTag = typeof tagArray === 'undefined' || Array.isArray(tagArray) && tagArray.length === 0;
        if (hasValueInTag) return 'NONE';
        var createTag = function createTag(tagElem) {
            return tagElem.tag ? tagElem['tag']['key'] + " = " + tagElem['tag']['value'] + ", " : tagElem['key'] + " = " + tagElem['value'] + ", ";
        };
        return tagArray.reduce(function (tagsToString, tagElem) {
            return tagsToString + createTag(tagElem);
        }, '').replace(/, $/, '') || 'NONE';
    };
    Scheme.prototype.getSchemeKeys = function () {
        return this.schemeKeys;
    };
    Scheme.prototype.getValues = function () {
        return this.schemeValues;
    };
    return Scheme;
}();
exports.default = Scheme;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var SeverityLevels = function () {
    function SeverityLevels() {
        this.sum = 0;
        this.high = 0;
        this.medium = 0;
        this.low = 0;
    }
    SeverityLevels.prototype.has = function (severityLevel) {
        return typeof this[severityLevel] !== 'undefined';
    };
    SeverityLevels.prototype.increaseCountBy = function (severityLevel, value) {
        if (typeof value === 'undefined') value = 0;
        this[severityLevel] += value;
        this.sum += value;
    };
    return SeverityLevels;
}();
exports.default = SeverityLevels;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

(function(global) {
  'use strict';

  var dateFormat = (function() {
      var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZWN]|'[^']*'|'[^']*'/g;
      var timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g;
      var timezoneClip = /[^-+\dA-Z]/g;
  
      // Regexes and supporting functions are cached through closure
      return function (date, mask, utc, gmt) {
  
        // You can't provide utc if you skip other args (use the 'UTC:' mask prefix)
        if (arguments.length === 1 && kindOf(date) === 'string' && !/\d/.test(date)) {
          mask = date;
          date = undefined;
        }
  
        date = date || new Date;
  
        if(!(date instanceof Date)) {
          date = new Date(date);
        }
  
        if (isNaN(date)) {
          throw TypeError('Invalid date');
        }
  
        mask = String(dateFormat.masks[mask] || mask || dateFormat.masks['default']);
  
        // Allow setting the utc/gmt argument via the mask
        var maskSlice = mask.slice(0, 4);
        if (maskSlice === 'UTC:' || maskSlice === 'GMT:') {
          mask = mask.slice(4);
          utc = true;
          if (maskSlice === 'GMT:') {
            gmt = true;
          }
        }
  
        var _ = utc ? 'getUTC' : 'get';
        var d = date[_ + 'Date']();
        var D = date[_ + 'Day']();
        var m = date[_ + 'Month']();
        var y = date[_ + 'FullYear']();
        var H = date[_ + 'Hours']();
        var M = date[_ + 'Minutes']();
        var s = date[_ + 'Seconds']();
        var L = date[_ + 'Milliseconds']();
        var o = utc ? 0 : date.getTimezoneOffset();
        var W = getWeek(date);
        var N = getDayOfWeek(date);
        var flags = {
          d:    d,
          dd:   pad(d),
          ddd:  dateFormat.i18n.dayNames[D],
          dddd: dateFormat.i18n.dayNames[D + 7],
          m:    m + 1,
          mm:   pad(m + 1),
          mmm:  dateFormat.i18n.monthNames[m],
          mmmm: dateFormat.i18n.monthNames[m + 12],
          yy:   String(y).slice(2),
          yyyy: y,
          h:    H % 12 || 12,
          hh:   pad(H % 12 || 12),
          H:    H,
          HH:   pad(H),
          M:    M,
          MM:   pad(M),
          s:    s,
          ss:   pad(s),
          l:    pad(L, 3),
          L:    pad(Math.round(L / 10)),
          t:    H < 12 ? 'a'  : 'p',
          tt:   H < 12 ? 'am' : 'pm',
          T:    H < 12 ? 'A'  : 'P',
          TT:   H < 12 ? 'AM' : 'PM',
          Z:    gmt ? 'GMT' : utc ? 'UTC' : (String(date).match(timezone) || ['']).pop().replace(timezoneClip, ''),
          o:    (o > 0 ? '-' : '+') + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
          S:    ['th', 'st', 'nd', 'rd'][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10],
          W:    W,
          N:    N
        };
  
        return mask.replace(token, function (match) {
          if (match in flags) {
            return flags[match];
          }
          return match.slice(1, match.length - 1);
        });
      };
    })();

  dateFormat.masks = {
    'default':               'ddd mmm dd yyyy HH:MM:ss',
    'shortDate':             'm/d/yy',
    'mediumDate':            'mmm d, yyyy',
    'longDate':              'mmmm d, yyyy',
    'fullDate':              'dddd, mmmm d, yyyy',
    'shortTime':             'h:MM TT',
    'mediumTime':            'h:MM:ss TT',
    'longTime':              'h:MM:ss TT Z',
    'isoDate':               'yyyy-mm-dd',
    'isoTime':               'HH:MM:ss',
    'isoDateTime':           'yyyy-mm-dd\'T\'HH:MM:sso',
    'isoUtcDateTime':        'UTC:yyyy-mm-dd\'T\'HH:MM:ss\'Z\'',
    'expiresHeaderFormat':   'ddd, dd mmm yyyy HH:MM:ss Z'
  };

  // Internationalization strings
  dateFormat.i18n = {
    dayNames: [
      'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
      'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ],
    monthNames: [
      'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
      'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ]
  };

function pad(val, len) {
  val = String(val);
  len = len || 2;
  while (val.length < len) {
    val = '0' + val;
  }
  return val;
}

/**
 * Get the ISO 8601 week number
 * Based on comments from
 * http://techblog.procurios.nl/k/n618/news/view/33796/14863/Calculate-ISO-8601-week-and-year-in-javascript.html
 *
 * @param  {Object} `date`
 * @return {Number}
 */
function getWeek(date) {
  // Remove time components of date
  var targetThursday = new Date(date.getFullYear(), date.getMonth(), date.getDate());

  // Change date to Thursday same week
  targetThursday.setDate(targetThursday.getDate() - ((targetThursday.getDay() + 6) % 7) + 3);

  // Take January 4th as it is always in week 1 (see ISO 8601)
  var firstThursday = new Date(targetThursday.getFullYear(), 0, 4);

  // Change date to Thursday same week
  firstThursday.setDate(firstThursday.getDate() - ((firstThursday.getDay() + 6) % 7) + 3);

  // Check if daylight-saving-time-switch occured and correct for it
  var ds = targetThursday.getTimezoneOffset() - firstThursday.getTimezoneOffset();
  targetThursday.setHours(targetThursday.getHours() - ds);

  // Number of weeks between target Thursday and first Thursday
  var weekDiff = (targetThursday - firstThursday) / (86400000*7);
  return 1 + Math.floor(weekDiff);
}

/**
 * Get ISO-8601 numeric representation of the day of the week
 * 1 (for Monday) through 7 (for Sunday)
 * 
 * @param  {Object} `date`
 * @return {Number}
 */
function getDayOfWeek(date) {
  var dow = date.getDay();
  if(dow === 0) {
    dow = 7;
  }
  return dow;
}

/**
 * kind-of shortcut
 * @param  {*} val
 * @return {String}
 */
function kindOf(val) {
  if (val === null) {
    return 'null';
  }

  if (val === undefined) {
    return 'undefined';
  }

  if (typeof val !== 'object') {
    return typeof val;
  }

  if (Array.isArray(val)) {
    return 'array';
  }

  return {}.toString.call(val)
    .slice(8, -1).toLowerCase();
};



  if (true) {
    !(__WEBPACK_AMD_DEFINE_RESULT__ = function () {
      return dateFormat;
    }.call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if (typeof exports === 'object') {
    module.exports = dateFormat;
  } else {
    global.dateFormat = dateFormat;
  }
})(this);


/***/ })
/******/ ]);
//# sourceMappingURL=jsrunner.js.map