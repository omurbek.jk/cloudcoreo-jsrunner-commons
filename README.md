Description
-----------
Generate HTMLReports for the email reports of CloudCoreo audit composites. Our module offers following methods:
- createEmails(JSON_INPUT, SETTINGS);

createEmails() method generates HTML report from raw data for particular audit composite.


How to install
-------

```bash
npm install cloudcoreo-jsrunner-commons
```

How to use
-----

```javascript
/**
* START VARIABLE FOR MODULE
*/

let alertListArray = ["ec2-get-all-instances-older-than", "rds-short-backup-retention-period"];
let disabledArray = "['ec2-get-all-instances-older-than', 'rds-short-backup-retention-period', 'ec2-get-all-instances-older-2']";


function createConfig(email, cloudObjects, userSuppression, ownerTAG, userSchemes, alertListArray, disabledArray) {
    let JSON_INPUT = {
        "composite name": "Composite Name",
        "plan name": "Plan Name",
        cloudObjects, userSchemes,
        userSuppression, 'alertList':alertListArray,
        "disabled": disabledArray
    };
    let SETTINGS = {
        NO_OWNER_EMAIL: email, OWNER_TAG: ownerTAG,
        ALLOW_EMPTY: "true", SEND_ON: "always",
        SHOWN_NOT_SORTED_VIOLATIONS_COUNTER: false
    };
    return {JSON_INPUT, SETTINGS};
}

const {JSON_INPUT, SETTINGS} = createConfig(
    'mihail@cloudcoreo',
    violations,
    suppression_input,
    'NOT_A_TAG',
    table_input,
    alertListArray,
    disabledArray
);

/**
* END VARIABLE FOR MODULE 
*/


/**
* HOW TO USE
* create JSRunnerModule
* and use method
* example:
*/

const CloudCoreoJSRunner = JSRunner.CloudCoreoJSRunner;
const emails = CloudCoreoJSRunner.createEmails(JSON_INPUT, SETTINGS);


/**
* END HOW TO USE
*/


```


