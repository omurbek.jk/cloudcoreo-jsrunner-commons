const CONSTANTS = {
    SEVERITY_COLORS: {
        HIGH: '#E53E2B',
        MEDIUM: '#E49530',
        LOW: '#6b6b6b',
        ANY: '#6b6b6b',
        COUNT: '#ffffff',
    },
    SCHEME_FOR_TABLE: {
        DEFAULT: {
            'AWS Object ID': '__OBJECT__',
            'AWS Region': '+__OBJECT__.violations.__RULE__.region+',
            'AWS Tags': '__TAGS__',
        },
    },
    IS_VISIBLE_SEVERITY_LEVEL: {
      INTERNAL: false,
    },
    WIDTH_FOR_ONE_COLUMN: 100,
    COLORS: {
        WHITE: '#ffffff',
        GREY: '#6b6b6b',
    },
    SEVERITY_LEVELS: {
        INFORMATIONAL: 'informational',
    },
    TEXT: {
        RULE: 'rule',
        RULES: 'rules',
    },
};
export default CONSTANTS;
