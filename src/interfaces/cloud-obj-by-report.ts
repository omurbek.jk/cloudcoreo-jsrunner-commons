import TagInterface from './tag';
import ObjByReportInterface from './obj-by-report';

interface CloudObjByReportInterface {
    violator_info?: {
        prop1?:any,
        prop2?:any,
    };
    violations?: {
        rule1?: ObjByReportInterface,
        rule2?: ObjByReportInterface,
    };
    tags:TagInterface[] | null;
}
export default CloudObjByReportInterface;
