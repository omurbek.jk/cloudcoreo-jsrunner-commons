import UserSchemesInterface from './user-schemes';
import UserSuppressionInterface from './user-suppressions';
import RuleInputsInterface from './rule-inputs';

interface CompositeReportInterface {
    compositeName?: string;
    planName?: string;
    teamName?: string;
    cloudAccount?: string;
    violations?: { string, any };
    alertList?: string[];
    userSchemes?: UserSchemesInterface;
    userSuppression?: UserSuppressionInterface;
    disabled?: RuleInputsInterface;
}
export default CompositeReportInterface;
