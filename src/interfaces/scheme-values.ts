interface SchemeValuesInterface {
    schemeValue: string | Date;
    hrefForValue: string;
}
export default SchemeValuesInterface;
