export type StringOrUndefined = string | undefined;
export type NumberOrUndefined = number | undefined;
