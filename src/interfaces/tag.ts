interface TagInterface {
    resource_id?: string;
    resource_type?: string;
    key?: string;
    value?: string;
    tag?: {
        key?: string,
        value?: string,
    };
}
export default TagInterface;
