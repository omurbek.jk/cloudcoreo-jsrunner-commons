type Column = { columnName?: string };
type ruleScheme = { ruleScheme?: Column };

interface UserSchemesInterface {
    rule1?: ruleScheme;
    rule2?: ruleScheme;
    rule3?: ruleScheme;
}

export default UserSchemesInterface;
