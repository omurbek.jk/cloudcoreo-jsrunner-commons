interface CompositePropInterface {
    NO_OWNER_EMAIL: string;
    OWNER_TAG: string;
    SEND_ON: boolean;
    ALLOW_EMPTY: string;
    SHOWN_NOT_SORTED_VIOLATIONS_COUNTER: boolean;
}
export default CompositePropInterface;
