import CloudObjByReportInterface from './cloud-obj-by-report';

interface ViolationsInterface {
    region1?: {
        cloudObjId1?: CloudObjByReportInterface,
        cloudObjId2?: CloudObjByReportInterface,
    };
    region2?: {
        cloudObjId1?: CloudObjByReportInterface,
        cloudObjId2?: CloudObjByReportInterface,
    };
}

export default ViolationsInterface;
