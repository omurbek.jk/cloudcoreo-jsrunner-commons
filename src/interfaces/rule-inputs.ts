export type ruleValue = {
    display_name?: {
        value: string, type: string,
    },
    link?: {
        value: string, type: string,
    }
    valueName?: {
        value: string, type: string,
    },
};


interface RuleInputsInterface {
    rule1?: {
        ruleValue1?: ruleValue,
        ruleValue2?: ruleValue,
        ruleValue4?: ruleValue,
    };
    rule2?: {
        ruleValue1?: ruleValue,
        ruleValue2?: ruleValue,
        ruleValue4?: ruleValue,
    };
}

export default RuleInputsInterface;
