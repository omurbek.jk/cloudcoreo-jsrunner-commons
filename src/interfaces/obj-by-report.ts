type resultInfo = {
    place?:string,
    object?: {
        someProp1?: any,
        someProp2?: any,
    };
};

interface ObjByReportInterface {
    service?:string;
    display_name?: string;
    result_info?: resultInfo[];
    description?:string;
    category?:string;
    suggested_action?:string;
    level?:string;
    link?:string;
    include_violations_in_count?:string;
    region?:string;
    resource_id?: string;
}
export default ObjByReportInterface;
