type suppressionValue = {cloudObjId?: string};
type suppressions = {rule?: suppressionValue[]};

interface UserSuppressionInterface {
    rule1?: suppressions;
    rule2?: suppressions;
    rule3?: suppressions;
}

export default UserSuppressionInterface;
