import CloudObject from '../models/cloud-object';
import CompositeReportInterface from '../interfaces/composite-report';
import CompositePropInterface from '../interfaces/composite-prop';
import CloudObjByReportInterface from '../interfaces/cloud-obj-by-report';
import { CloudObjTypeValues } from '../models/cloud-object-type';
import { StringOrUndefined } from '../interfaces/basic-types';

export default class SuppressionJSON {
    constructor() {
    }

    public static build(compositeReport: CompositeReportInterface,
                        userProperties: CompositePropInterface): CompositeReportInterface {
        Object.keys(compositeReport.violations).forEach(region => {
            Object.keys(compositeReport.violations[region]).forEach((cloudObjId) => {
                const violationWay: CloudObjByReportInterface = compositeReport.violations[region][cloudObjId].violations;
                Object.keys(violationWay).forEach(ruleId => {
                    const cloudObjectArg = { region, cloudObjId, ruleId };
                    const cloudObject: CloudObject = new CloudObject(cloudObjectArg, compositeReport, userProperties);
                    const suppressionType: CloudObjTypeValues = cloudObject.getType();
                    const suppressionData: Date | StringOrUndefined = cloudObject.getSuppressionData();
                    if (suppressionData) {
                        violationWay[ruleId].suppression_until = suppressionData;
                        if (suppressionType === 'suppressed') {
                            violationWay[ruleId].suppressed = 'true';
                        }
                    }
                });
            });
        });
        return compositeReport;
    }
}
