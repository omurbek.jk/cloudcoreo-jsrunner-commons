import Cards from '../models/cards';
import CloudObject from '../models/cloud-object';
import CompositePropInterface from '../interfaces/composite-prop';
import CompositeReportInterface from '../interfaces/composite-report';
import CloudObjByReportInterface from '../interfaces/cloud-obj-by-report';

export default class CardBuilder {
    constructor() {
    }

    /**
     * Build cards
     * @param compositeReport
     * @param userProperties
     * @returns {Cards}
     */
    public static build(compositeReport:CompositeReportInterface, userProperties: CompositePropInterface): Cards {
        const cards: Cards = new Cards();
        Object.keys(compositeReport.violations).forEach(region => {
            Object.keys(compositeReport.violations[region]).forEach(cloudObjId => {
                const violationWay: CloudObjByReportInterface = compositeReport.violations[region][cloudObjId];
                Object.keys(violationWay.violations).forEach(ruleId => {
                    const cloudObjectArg = { region, cloudObjId, ruleId };
                    cards.add(new CloudObject(cloudObjectArg, compositeReport, userProperties));
                });
            });
        });
        return cards;
    }
}
