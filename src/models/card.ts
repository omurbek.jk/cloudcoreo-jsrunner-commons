import CloudObject from './cloud-object';
import CardHTML from '../factories/card-html';
import {NumberOrUndefined} from '../interfaces/basic-types';

export default class Card extends CardHTML {
    card: {
        violations: CloudObject[],
        suppressed: CloudObject[],
    };

    constructor() {
        super();
        this.card = {
            violations: [],
            suppressed: [],
        };
    }

    public getViolationLength(): number {
        return this.card.violations.length;
    }

    /**
     * Push CloudObject to Card by CloudObjectType
     * @param cloudObject
     */
    public add(cloudObject: CloudObject): void {
        this.card[cloudObject.getType()].push(cloudObject);
    }

    public hasSuppressDate(): boolean {
        const hasDateInViolations: boolean = this.card.violations.some(cloudObj => cloudObj.isShownDate());
        const hasDateInSuppression: boolean = this.card.suppressed.some(cloudObj => cloudObj.isShownDate());
        return hasDateInViolations || hasDateInSuppression;
    }

    private setSuppressUntilKeyForTableHeaderBy(key: string): void {
        this.card[key].forEach(cloudObj => {
            cloudObj.addSuppressUntilSloganForColumn();
            cloudObj.pushSuppressDateToSchemeValues();
        });
    }

    public setSuppressUntilKeyForTableHeader(): void {
        this.setSuppressUntilKeyForTableHeaderBy('violations');
        this.setSuppressUntilKeyForTableHeaderBy('suppressed');
    }

    public getRuleId(): string {
        return this.card.violations.length > 0 ? this.card.violations[0].ruleId : this.card.suppressed[0].ruleId;
    }

    public getSortedCardByEmail(): any {
        const sortedCard = new Map();
        this.applyCloudObject(sortedCard, 'violations');
        this.applyCloudObject(sortedCard, 'suppressed');
        return sortedCard;
    }

    public getLengthViolations(): NumberOrUndefined {
        return this.card.violations.length;
    }

    public getSeverityLevel(): string {
        const firstObj: CloudObject = this.card.violations[0];
        if (!firstObj) return '';
        return firstObj.getSeverityLevel();
    }

    private applyCloudObject(sortedCard: any, cloudObjectType: string): void {
        this.card[cloudObjectType].forEach(cloudObject => {
            const notificationEmail: string = cloudObject.getNotificationEmail();
            if (!sortedCard.has(notificationEmail)) sortedCard.set(notificationEmail, new Card());
            sortedCard.get(notificationEmail).add(cloudObject);
        });
    }
}
