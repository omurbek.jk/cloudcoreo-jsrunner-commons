import CloudObjByReportInterface from '../interfaces/cloud-obj-by-report';
import TagInterface from '../interfaces/tag';
import { StringOrUndefined } from '../interfaces/basic-types';
type NotificationType = 'userEmail' | 'tagEmail' | 'noOwnerEmail' | undefined;
type EmailFromTag = StringOrUndefined;
type ValueForTable = StringOrUndefined;

export default class EmailType {
    violations: CloudObjByReportInterface;
    ruleId: string;
    tags: TagInterface[] | null | undefined;
    OWNER_TAG: string;
    NO_OWNER_EMAIL: string;
    cloudObjId: string;
    emailType: NotificationType;
    emailFromTag: EmailFromTag;
    notificationEmail: string;
    valueForTable: ValueForTable;

    constructor(emailProperties) {
        this.violations = emailProperties.violations;
        this.tags = emailProperties.tags;
        this.ruleId = emailProperties.ruleId;
        this.cloudObjId = emailProperties.cloudObjId;
        this.OWNER_TAG = emailProperties.compositeProp.OWNER_TAG;
        this.NO_OWNER_EMAIL = emailProperties.compositeProp.NO_OWNER_EMAIL;
        this.setEmailFromTag();
        this.setNotificationType();
        this.setNotificationEmail();
        this.setValueForTable();
    }

    private setEmailFromTag(): void {
        const tagsIsUndefined = typeof this.tags == 'undefined';
        const tagsIsNull = this.tags == null;

        if (tagsIsUndefined || tagsIsNull) return;
        this.tags.forEach(tag => {
            if (tag.hasOwnProperty('tag')) tag = tag['tag'];
            const hasEmail: boolean = tag.key === this.OWNER_TAG;
            if (hasEmail) this.emailFromTag = tag.value;
        });
        const tagsIsArray = this.tags && Array.isArray(this.tags)
            && this.tags.length > 0 && typeof this.emailFromTag === 'undefined';
        if (tagsIsArray) this.emailFromTag = '';
    }

    private setNotificationType(): void {
        const isDefaultOwnerTag: boolean = this.OWNER_TAG === 'NOT_A_TAG';
        if (isDefaultOwnerTag) {
            this.emailType = 'userEmail';
            return;
        }
        const hasEmail = typeof this.emailFromTag !== 'undefined';
        const isDifferentEmail = this.NO_OWNER_EMAIL !== this.emailFromTag;
        const isEmptyEmail = this.emailFromTag !== '';
        const isTagEmail: boolean = hasEmail && isDifferentEmail && isEmptyEmail;
        this.emailType = isTagEmail ? 'tagEmail' : 'noOwnerEmail';
    }

    private setNotificationEmail(): void {
        const isNoOwnerEmail = this.emailType === 'userEmail' || this.emailType === 'noOwnerEmail';
        this.notificationEmail = isNoOwnerEmail ? this.NO_OWNER_EMAIL : this.emailFromTag;
    }

    private setValueForTable(): void {
        const isNoOwner: boolean = this.emailType === 'noOwnerEmail';
        const isTagEmail = !isNoOwner && this.emailType === 'tagEmail';
        if (isNoOwner) {
            this.valueForTable = 'No Owner';
            return;
        }
        if (isTagEmail) this.valueForTable = this.emailFromTag;
    }

    public getNotificationEmail(): string {
        return this.notificationEmail;
    }

    public getValueForTable(): string {
        return this.valueForTable;
    }

    public isShownEmail(): boolean {
        return !!this.valueForTable;
    }
}
