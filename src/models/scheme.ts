import CloudObjectType from './cloud-object-type';
import EmailType from './email-type';
import Constants from '../constants';
import TagInterface from '../interfaces/tag';
import UserSchemesInterface from '../interfaces/user-schemes';
import CloudObjByReportInterface from '../interfaces/cloud-obj-by-report';
import SchemeValuesInterface from '../interfaces/scheme-values';

type UserScheme = { columnTitle?: string };
type TagsType = TagInterface[] | null;

export default class Scheme {
    userSchemes: UserSchemesInterface;
    violations: CloudObjByReportInterface;
    ruleId: string;
    tags: TagsType;
    cloudObjId: string;
    cloudObjType: CloudObjectType;
    emailType: EmailType;
    userScheme: UserScheme;
    schemeKeys: string[] | undefined;
    schemeValues: SchemeValuesInterface[];

    constructor(properties) {
        this.userSchemes = properties.userSchemes;
        this.violations = properties.violations;
        this.ruleId = properties.ruleId;
        this.cloudObjId = properties.cloudObjId;
        this.tags = properties.tags;
        this.cloudObjType = properties.cloudObjType;
        this.emailType = properties.emailType;
        this.setUserScheme();
        this.setSchemeKeys();
        this.setSchemeValues();
        this.setOwnerEmail();
    }

    private setUserScheme(): void {

        if (this.userSchemes && this.userSchemes[this.ruleId]) {
            this.userScheme = this.userSchemes[this.ruleId];
        } else if (this.userSchemes && this.userSchemes['default']) {
            this.userScheme = this.userSchemes['default'];
        } else {
            this.userScheme = Constants.SCHEME_FOR_TABLE.DEFAULT;
        }

    }

    private setSchemeKeys(): void {
        if (!this.userScheme) return;
        this.schemeKeys = Object.keys(this.userScheme);
    }

    private setSchemeValues(): void {
        this.schemeValues = [];
        if (!(this.schemeKeys && this.schemeKeys.length)) return;
        this.schemeKeys.forEach(schemeKey => {
            const scheme: string = this.replaceTagKeys(this.userScheme[schemeKey], this.tags);
            const replacedScheme: string = this.replaceScheme(scheme, this.violations);
            const hrefForValue: string = this.createLinkBy(replacedScheme);
            const schemeValue: string = this.replaceComponentArrowFor(this.createSchemeValue(replacedScheme));
            this.schemeValues.push({schemeValue, hrefForValue});
        });
    }

    private replaceComponentArrowFor = (value: string): string => value.replace(/[<>]/g, '');

    private replaceTagKeys(valuePath: string, tags: TagsType): string {
        valuePath = this.replaceForValue(valuePath, '__TAGKEYS__', this.createTagsStr(tags));
        return this.replaceForValue(valuePath, '__TAGS__', this.createTagsForBuilder(tags));
    }

    private replaceForValue(valuePath: string, thatChange: string, toChangeThat: string): string {
        if (valuePath.indexOf(thatChange) === -1) return valuePath;
        return valuePath.replace(thatChange, toChangeThat);
    }

    private replaceScheme(scheme: string, cloudObjects: CloudObjByReportInterface): string {
        const regExpForSchemePart: RegExp = /\+[a-zA-Z0-9\-\._\/]+\+/g;
        const schemeParts = scheme.match(regExpForSchemePart);
        if (schemeParts) {
            schemeParts.forEach(schemePart => {
                const valuePart = this.getValueBy(schemePart, cloudObjects);
                scheme = scheme.replace(schemePart, valuePart);
            });
        }
        return scheme
            .replace(/__OBJECT__/g, this.cloudObjId)
            .replace(/__RULE__/g, this.ruleId);
    }

    private createLinkBy(scheme: string): string {
        const indexTilde = scheme.indexOf('~');
        if (indexTilde >= 0) return scheme.substring(indexTilde + 1, scheme.length);
        return '';
    }

    private createSchemeValue(scheme: string): string {
        const indexForHref = scheme.indexOf('~');
        if (indexForHref !== -1) return scheme.substring(0, indexForHref);
        return scheme;
    }

    private getValueBy(schemePart: string, cloudObjects: CloudObjByReportInterface): string {
        schemePart = schemePart.replace(/\+/g, '');
        schemePart.split('.').some(valuePath => {
            if (valuePath === '__OBJECT__') valuePath = this.cloudObjId;
            if (valuePath === '__RULE__') valuePath = this.ruleId;
            const hasValue = typeof cloudObjects === 'string';
            if (hasValue) return true;
            const hasCorrectPath = cloudObjects.hasOwnProperty(valuePath);
            if (hasCorrectPath) cloudObjects = cloudObjects[valuePath];
        });
        const hasCorrectValue: boolean = typeof cloudObjects === 'string'
            || typeof cloudObjects === 'number'
            || typeof cloudObjects === 'boolean';
        let value = (hasCorrectValue) ? cloudObjects.toString() : 'NONE';
        const isIAMLink = value.indexOf('arn:aws:iam') === -1 && value.indexOf('arn:aws') >= 0;
        if (isIAMLink) value = value.replace('/', '@');
        return value;
    }

    private setOwnerEmail(): void {
        if (!this.emailType.isShownEmail()) return;
        this.schemeKeys.push('Owner');
        const schemeValue: string = this.emailType.getValueForTable();
        let hrefForValue: string = `mailto:${schemeValue}`;
        if (schemeValue === 'No Owner') hrefForValue = '';
        this.schemeValues.push({schemeValue, hrefForValue});
    }

    private createTagsStr(tagArray: TagsType): string {
        const hasValueInTag: boolean = typeof tagArray === 'undefined'
            || (Array.isArray(tagArray) && tagArray.length === 0);
        if (hasValueInTag) return 'NONE';
        const createTag = (tagElem) => tagElem.tag ? `${tagElem['tag']['key']}, ` : `${tagElem['key']}, `;
        return tagArray
                .reduce((tagsToString, tagElem) => tagsToString + createTag(tagElem), '')
                .replace(/, $/, '') || 'NONE';
    }

    private createTagsForBuilder(tagArray: TagsType): string {
        const hasValueInTag: boolean = typeof tagArray === 'undefined'
            || (Array.isArray(tagArray) && tagArray.length === 0);
        if (hasValueInTag) return 'NONE';
        const createTag = (tagElem) => tagElem.tag ?
            `${tagElem['tag']['key']} = ${tagElem['tag']['value']}, ` :
            `${tagElem['key']} = ${tagElem['value']}, `;
        return tagArray
                .reduce((tagsToString, tagElem) => tagsToString + createTag(tagElem), '')
                .replace(/, $/, '') || 'NONE';
    }

    public getSchemeKeys(): string[] {
        return this.schemeKeys;
    }

    public getValues(): SchemeValuesInterface[] {
        return this.schemeValues;
    }

}
