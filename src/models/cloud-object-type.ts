import UserSuppressionInterface from '../interfaces/user-suppressions';
import UserSchemesInterface from '../interfaces/user-schemes';
import { StringOrUndefined } from '../interfaces/basic-types';
import * as dateformat from 'dateformat';

export type CloudObjTypeValues = 'suppressed' | 'violations';

export default class CloudObjectType {
    userSuppression: UserSuppressionInterface;
    userProperties: UserSchemesInterface;
    ruleId: string;
    cloudObjId: string;
    userSuppressionDate: StringOrUndefined;
    correctSuppressDate: Date | undefined;
    type: CloudObjTypeValues;
    transformDate: StringOrUndefined;

    constructor(propertiesForCloudObjType) {
        this.userSuppression = propertiesForCloudObjType.userSuppression;
        this.userProperties = propertiesForCloudObjType.compositeProp;
        this.ruleId = propertiesForCloudObjType.ruleId;
        this.cloudObjId = propertiesForCloudObjType.cloudObjId;
        this.setUserSuppressionDate();
        this.setCorrectSuppressDate();
        this.setCloudObjectType();
        this.setTransformDate();
    }

    private setUserSuppressionDate(): void {
        if (!this.userSuppression) return;
        const userSuppression = this.userSuppression[this.ruleId];
        if (typeof userSuppression === 'undefined') return;
        const suppression = this.userSuppression[this.ruleId]
            .find((suppression => suppression.hasOwnProperty(this.cloudObjId)));
        if (suppression && suppression.hasOwnProperty(this.cloudObjId)) {
            this.userSuppressionDate = suppression[this.cloudObjId];
        }
    }

    private setCorrectSuppressDate(): void {
        if (this.userSuppressionDate === '') return;
        this.correctSuppressDate = new Date(this.userSuppressionDate);
    }

    private setCloudObjectType(): void {
        const nowDate = new Date();
        const isSuppressed: boolean = nowDate <= this.correctSuppressDate;
        this.type = isSuppressed || !this.correctSuppressDate ? 'suppressed' : 'violations';
    }

    private setTransformDate(): void {
        if (!this.userSuppressionDate) return;
        const suppressionDate: Date = new Date(this.userSuppressionDate);
        this.transformDate = (dateformat.default) ? dateformat.default(suppressionDate, 'dd/mm/yyyy hh:MM') : dateformat(suppressionDate, 'dd/mm/yyyy hh:MM');
    }

    public getUserSuppressionDate(): StringOrUndefined {
        return this.userSuppressionDate;
    }

    public getCorrectSuppressDate(): undefined | Date {
        return this.correctSuppressDate;
    }

    public getType(): CloudObjTypeValues {
        return this.type;
    }

    public getTransformDate(): undefined | string | Date {
        return this.transformDate;
    }

    public isShownDate(): boolean {
        return typeof this.userSuppressionDate !== 'undefined';
    }
}
