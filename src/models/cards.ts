import Card from "./card";
import CloudObject from "./cloud-object";
import CardsHTML from "../factories/cards-html";

export default class Cards extends CardsHTML {

    constructor() {
        super();
        this.cards = new Map();
    }

    public setTableKeysForCards(): void {
        this.cards.forEach(card => {
            if (card.hasSuppressDate()) card.setSuppressUntilKeyForTableHeader()
        });
    }

    public add(cloudObject: CloudObject): void {
        if (this.has(cloudObject.ruleId)) {
            this.getCardBy(cloudObject.ruleId).add(cloudObject);
            return;
        }
        const card: Card = new Card();
        card.add(cloudObject);
        this.create(cloudObject.ruleId, card);
    }

    public sortCardsBySeverityLevel(): void {
        const prioritySort = { high: [], medium: [], low: [], anything: [] };
        this.cards.forEach((card: Card, rule: string) => {
            const severityLevel: string = card.getSeverityLevel();
            if (prioritySort[severityLevel]) {
                prioritySort[severityLevel].push({ rule, card });
                return;
            }
            prioritySort.anything.push({ rule, card });
        });
        this.cards = new Map();
        Object.keys(prioritySort).forEach((priority: string) => {
            prioritySort[priority].forEach(cardObj => {
                const { rule, card } = cardObj;
                this.create(rule, card);
            });
        });
    }

    public getSortedCardsByEmail(): Map<string, Cards> {
        const sortedCardsByEmail: any = new Map();
        this.cards.forEach(card => {
            const cardSortedByEmail = card.getSortedCardByEmail();
            cardSortedByEmail.forEach((card, email) => {
                if (sortedCardsByEmail.has(email)) {
                    sortedCardsByEmail.get(email).create(card.getRuleId(), cardSortedByEmail.get(email));
                    return;
                }
                const cards: Cards = new Cards();
                cards.create(card.getRuleId(), cardSortedByEmail.get(email));
                sortedCardsByEmail.set(email, cards);
            });
        });
        return sortedCardsByEmail;
    }

    private has(rule: string): boolean {
        return this.cards.has(rule);
    }

    private create(rule: string, card: Card): void {
        this.cards.set(rule, card);
    }

    private getCardBy(rule: string): Card {
        return this.cards.get(rule);
    }

    public getLengthCards(): number {
        let counter:number = 0;
        this.cards.forEach(card => counter += card.getLengthViolations());
        return counter;
    }

}
