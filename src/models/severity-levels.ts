import { NumberOrUndefined } from '../interfaces/basic-types';
export default class SeverityLevels {
    sum: number;
    high: number;
    medium: number;
    low: number;

    constructor() {
        this.sum = 0;
        this.high = 0;
        this.medium = 0;
        this.low = 0;
    }

    public has(severityLevel: string): boolean {
        return typeof this[severityLevel] !== 'undefined';
    }

    public increaseCountBy(severityLevel: string, value: NumberOrUndefined): void {
        if (typeof value === 'undefined') value = 0;
        this[severityLevel] += value;
        this.sum += value;
    }
}
