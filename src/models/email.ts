import { argForEmail } from '../index';
export default class Email {
    type: string;
    send_on: string;
    allow_empty: string;
    payload_type: string;
    endpoint: {
        to: string,
        subject: string,
    };
    num_violations: string;
    num_instances: string;
    payload: string;
    numberOfViolatingCloudObjects:number;

    constructor(argsForEmail:argForEmail) {
        this.type = 'email';
        this.send_on = argsForEmail.compositeProp.SEND_ON.toString();
        this.allow_empty = argsForEmail.compositeProp.ALLOW_EMPTY.toString();
        this.payload_type = 'html';
        this.payload_type = 'html';
        this.endpoint = {
            to: argsForEmail.notificationEmail,
            subject: `[${argsForEmail.compositeReport.compositeName}] New Owner Tag Report for ${argsForEmail.compositeReport.planName} plan from CloudCoreo`,
        };
        this.num_violations = argsForEmail.cards.getLengthCards().toString();
        this.num_instances = argsForEmail.cards.getLengthCards().toString();
        this.numberOfViolatingCloudObjects = argsForEmail.cards.getNumberOfViolatingCloudObjects(argsForEmail.compositeReport);
        this.payload = argsForEmail.cards.getHTMLHeader(argsForEmail.compositeReport)
            + argsForEmail.cards.getHTMLBody()
            + argsForEmail.cards.getHTMLNoViolations(argsForEmail.compositeReport);
    }
}
