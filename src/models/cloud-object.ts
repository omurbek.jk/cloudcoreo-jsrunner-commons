import EmailType from './email-type';
import CloudObjType, { CloudObjTypeValues } from './cloud-object-type';
import Scheme from './scheme';
import CONSTANTS from '../constants';
import CompositeReportInterface from '../interfaces/composite-report';
import CompositePropInterface from '../interfaces/composite-prop';
import UserSuppressionInterface from '../interfaces/user-suppressions';
import UserSchemesInterface from '../interfaces/user-schemes';
import ObjByReportInterface from '../interfaces/obj-by-report';
import SchemeValuesInterface from '../interfaces/scheme-values';
import { StringOrUndefined } from '../interfaces/basic-types';

type CloudObjectArg = { region: string, cloudObjId: string, ruleId: string };

export default class CloudObject {
    cloudObjectArg: CloudObjectArg;
    compositeReport: CompositeReportInterface;
    compositeProp: CompositePropInterface;
    userSuppression: UserSuppressionInterface;
    userSchemes: UserSchemesInterface;
    region: string;
    cloudObjId: string;
    ruleId: string;
    objByReport: ObjByReportInterface;
    cloudObjType: CloudObjType;
    emailType: EmailType;
    scheme: Scheme;

    constructor(cloudObjectArg: CloudObjectArg, compositeReport: CompositeReportInterface, userProperties: CompositePropInterface) {
        this.cloudObjectArg = cloudObjectArg;
        this.region = cloudObjectArg.region;
        this.cloudObjId = cloudObjectArg.cloudObjId;
        this.ruleId = cloudObjectArg.ruleId;
        this.compositeReport = compositeReport;
        this.userSuppression = compositeReport.userSuppression;
        this.userSchemes = compositeReport.userSchemes;
        this.compositeProp = userProperties;
        this.objByReport = this.compositeReport.violations[this.region][this.cloudObjId].violations[this.ruleId];
        this.objByReport.resource_id = this.ruleId;
        this.setCloudObjType();
        this.setEmailType();
        this.setScheme();
    }

    private setCloudObjType(): void {
        const argsForCloudObjType = {
            userSuppression: this.userSuppression,
            userProperties: this.compositeProp,
            cloudObjId: this.cloudObjId, ruleId: this.ruleId,
        };
        this.cloudObjType = new CloudObjType(argsForCloudObjType);
    }

    private setEmailType(): void {
        const argsForEmailType = {
            violations: this.compositeReport.violations[this.region][this.cloudObjId],
            tags: this.compositeReport.violations[this.region][this.cloudObjId].tags,
            compositeProp: this.compositeProp,
            cloudObjId: this.cloudObjId, ruleId: this.ruleId,
        };
        this.emailType = new EmailType(argsForEmailType);
    }

    private setScheme(): void {
        const argsForScheme = {
            userSchemes: this.userSchemes,
            violations: this.compositeReport.violations[this.region],
            tags: this.compositeReport.violations[this.region][this.cloudObjId].tags,
            compositeProp: this.compositeProp,
            cloudObjId: this.cloudObjId, ruleId: this.ruleId,
            cloudObjType: this.cloudObjType, emailType: this.emailType,
        };
        this.scheme = new Scheme(argsForScheme);
    }

    public getType(): CloudObjTypeValues {
        return this.cloudObjType.getType();
    }

    public getNotificationEmail(): string {
        return this.emailType.getNotificationEmail();
    }

    public getSeverityLevel(): string {
        return this.objByReport.level.toLowerCase() || '';
    }

    public addSuppressUntilSloganForColumn(): void {
        this.scheme.schemeKeys.push('Suppression Until');
    }

    public pushSuppressDateToSchemeValues(): void {
        const schemeValue: string | Date = this.cloudObjType.getTransformDate() || '';
        const hrefForValue: string = '';
        this.scheme.schemeValues.push({ schemeValue, hrefForValue });
    }

    public getColor(): string {
        const levels = { high: CONSTANTS.SEVERITY_COLORS.HIGH, medium: CONSTANTS.SEVERITY_COLORS.MEDIUM, low: CONSTANTS.SEVERITY_COLORS.LOW };
        const countColor = CONSTANTS.SEVERITY_COLORS.COUNT;
        const level = this.objByReport.level.toLowerCase();
        if (!levels[level]) return countColor;
        return levels[level];
    }

    public isShownDate(): boolean {
        return this.cloudObjType.isShownDate();
    }

    public getDisplayName(): StringOrUndefined {
        return this.objByReport.display_name;
    }

    public getKBLink(): StringOrUndefined {
        return this.objByReport.link;
    }

    public getDescription(): string {
        return this.objByReport.description;
    }

    public getSuppressionData(): Date | undefined | string {
        return this.cloudObjType.getUserSuppressionDate();
    }

    public getTableKeys(): string[] {
        return this.scheme.getSchemeKeys();
    }

    public getValueForTable(): SchemeValuesInterface[] {
        return this.scheme.getValues();
    }

    public getResourceName():StringOrUndefined {
       return this.objByReport.resource_id;
    }

}
