import SeverityLevels from '../models/severity-levels';
import CONSTANTS from '../constants';
import Card from '../models/card';
import {NumberOrUndefined} from '../interfaces/basic-types';
import CompositeReportInterface from '../interfaces/composite-report';
import RuleInputsInterface, {ruleValue} from '../interfaces/rule-inputs';

export default class CardsHTML {
    cards: any;

    constructor() {
        this.cards = new Map;
    }

    private createSeverityLevel(): SeverityLevels {
        const severityLevels: SeverityLevels = new SeverityLevels();
        this.cards.forEach((card: Card) => {
            const severityLevel: string = card.getSeverityLevel();
            const violationLength: NumberOrUndefined = card.getLengthViolations();
            if (severityLevels.has(severityLevel)) {
                severityLevels.increaseCountBy(severityLevel, violationLength);
            }

        });
        return severityLevels;
    }

    private getLengthAlertLists(alertList: string[]): number {
        return alertList ? alertList.length : this.cards.size;
    }

    private getHTMLLogoWithAccount(): string {
        return `
            <div style="background: #fff;font-family:'Arial', sans-serif;">
                <div style="font-family:'Arial', sans-serif;text-align:center;background: #f2f2f2; padding: 15px 0 14px">
                    <a target="_blank" href="https://www.cloudcoreo.com/"><img src="https://appassets.cloudcoreo.com/img/logo/logo-alt-black.png" alt=""></a>
                </div>
                <!-- ACCOUNT_SUGGESTION_START -->
                <div style="font-family:'Arial', sans-serif;text-align:center; background:#e4e4e4; padding: 14px; font-size:16px;">
                    Don't have an account? <a href="https://www.cloudcoreo.com/early-access/">Sign up for early access.</a>
                </div>
                <!--ACCOUNT_SUGGESTION_END-->
            </div>
        `;
    }

    private getHTMLAuditSummarySlogan(compositeReport: CompositeReportInterface): string {
        let compositeAndPlan: string = '';
        let cloudAccount: string = '';
        let teamName: string = '';
        if (compositeReport.teamName) {
            teamName = `<p style="margin:0">Cloud Team: ${compositeReport.teamName}</p>`;
        }
        if (compositeReport.compositeName && compositeReport.planName) {
            compositeAndPlan = `<p style="margin:0">${compositeReport.compositeName}: ${compositeReport.planName}</p>`;
        }
        if (compositeReport.cloudAccount) {
            cloudAccount = `<p style="margin:0">Cloud account: ${compositeReport.cloudAccount}</p>`;
        }
        return `<div style="text-align:center;padding-top:30px;padding-bottom:15px;font-size:18px;color:#000;">
                    <p style="font-size:24px;margin:0;color:#000; font-weight:bold;">Audit Summary</p>
                    ${compositeAndPlan}
                    ${cloudAccount}
                    ${teamName}
                </div>`;
    }

    private generateHTMLForSeverityCounter(severityLevel: SeverityLevels, level: string): string {
        const levels = {
            high: CONSTANTS.SEVERITY_COLORS.HIGH,
            medium: CONSTANTS.SEVERITY_COLORS.MEDIUM,
            low: CONSTANTS.SEVERITY_COLORS.LOW,
        };
        let color: string = CONSTANTS.SEVERITY_COLORS.ANY;
        if (levels.hasOwnProperty(level)) {
            color = levels[level];
        }
        return `<div style="width:calc(100% / 3); height:90px;padding-top:10px;color:white;">
                    <div style="font-size:42px;color:${color};
                    margin-bottom: 6px;">${severityLevel[level]} </div>
                    <div style="padding:10px;background:${color};text-transform: capitalize;
                    ">${level} Severity</div>
                </div>`;
    }

    private getHTMLAuditSummaryBody(severityLevel: SeverityLevels): string {
        return `<div style="color:#000;font-size: 14px;flex-wrap:wrap;text-align: center;
                    font-weight: bold; padding:0 10px 10px;">
                    <div style="width:calc(100% - 40px);background:#fff;height:90px;padding: 10px 20px 0;">
                        <div style="font-size:42px;">${severityLevel.sum}</div>
                        Violating <br> Cloud Objects
                    </div>
                    <div style="display:flex;">
                        ${this.generateHTMLForSeverityCounter(severityLevel, 'high')}
                        ${this.generateHTMLForSeverityCounter(severityLevel, 'medium')}
                        ${this.generateHTMLForSeverityCounter(severityLevel, 'low')}
                    </div>
                </div>`;
    }

    private getHTMLAuditSummary(compositeReport: CompositeReportInterface): string {
        const severityLevel: SeverityLevels = this.createSeverityLevel();
        return `<div style="background:#f2f2f2;font-family:'Arial', 'sans-serif';margin-top:10px;">
                ${this.getHTMLAuditSummarySlogan(compositeReport)}
                ${this.getHTMLAuditSummaryBody(severityLevel)}
            </div>`;
    }

    public getNumberOfViolatingCloudObjects(compositeReport: CompositeReportInterface): number {
        const severityLevel: SeverityLevels = this.createSeverityLevel();
        return severityLevel.sum;
    }

    private getHTMLAuditDetails(compositeReport: CompositeReportInterface): string {
        const getTextRuleBy = (length: number) => length === 1 ? CONSTANTS.TEXT.RULE : CONSTANTS.TEXT.RULES;

        const alertListLength: number = this.getLengthAlertLists(compositeReport.alertList);
        const cardsCountForViolationFound: number = this.cards.size;

        const textForAlertList: string = getTextRuleBy(alertListLength);
        const textForCardsCount: string = getTextRuleBy(cardsCountForViolationFound);

        return `
            <div style="text-align:center; color:#000;font-size: 14px;padding: 10px 0;
            font-family: 'Arial', sans-serif;">
                <p style="font-weight: bold;font-size: 18px;margin: 0 3px;">Audit Details</p>
                <p style="font-style: italic;margin: 0 3px;">Cloud Objects List (by Rule)</p>
                <p style="margin: 0 3px;">${alertListLength} ${textForAlertList} run, ${cardsCountForViolationFound} ${textForCardsCount} found matching Cloud Objects
                </p>
            </div>
        `;
    }

    private getHTMLHeaderForNoViolations(): string {
        return `
            <div style="margin-left:10px;">
                <span style="font-family: Arial, sans-serif;padding: 5px;margin-bottom: 10px;font-size: 14px;
                    border:1px solid #e4e4e4; text-transform:capitalize;
                    display:inline-block;background:#f2f2f2;font-weight: bold;color:#6b6b6b;">No Violations</span>
            </div>
        `;
    }

    private getHTMLForDisabledViolations(disableViol: ruleValue): string {
        let HTML_FOR_DISABLED_VIOLATIONS: string = '';
        let link: string = '';
        let displayName: string = '';
        if (disableViol.display_name) {
            displayName = `<span style="font-size: 18px;color: #000; margin-right: 10px;">
                    ${disableViol.display_name.value}
                </span>`;
        }

        if (disableViol.link) {
            link = `<span style="margin-left:auto;"><a style="font-size: 14px;color:#2B7AE5;text-decoration:none;"
                        href="${disableViol.link.value}"> MORE INFO ›</a></span>`;
        }

        HTML_FOR_DISABLED_VIOLATIONS += `
            <div style="border:1px solid #d6d6d6; border-left:0; border-right:0;padding: 10px 0;
                display: flex;flex-wrap: wrap;align-items: center;
            margin-left: 10px;font-family: Arial, sans-serif;font-weight: bold;">
                   ${displayName}
                   ${link}
            </div>`;

        return HTML_FOR_DISABLED_VIOLATIONS;
    }

    public getHTMLHeader(compositeReport: CompositeReportInterface): string {
        let HTML_HEADER: string = this.getHTMLLogoWithAccount();
        HTML_HEADER += this.getHTMLAuditSummary(compositeReport);
        HTML_HEADER += this.getHTMLAuditDetails(compositeReport);
        return HTML_HEADER;
    }

    public getHTMLBody(): string {
        let HTML_BODY: string = '';
        this.cards.forEach(card => {
            if (CONSTANTS.IS_VISIBLE_SEVERITY_LEVEL[card.getSeverityLevel().toUpperCase()] === false) {
                return;
            }
            HTML_BODY += card.getHTMLForCard();
        });
        return HTML_BODY;
    }

    public getHTMLNoViolations(compositeReport: CompositeReportInterface): string {
        let HTML_NO_VIOLATIONS: string = '';
        let HTML_FOR_NO_VIOLATIONS: string = '';
        const disabled: RuleInputsInterface = compositeReport.disabled;
        this.cards.forEach(card => HTML_FOR_NO_VIOLATIONS += card.getHTMLForNoViolations());
        const alertLists = new Set(compositeReport.alertList);
        Object.keys(disabled).forEach(rule => {
            const disableViol: ruleValue = disabled[rule];
            if (!alertLists.has(rule)) {
                HTML_FOR_NO_VIOLATIONS += this.getHTMLForDisabledViolations(disableViol);
            }
        });
        if (HTML_FOR_NO_VIOLATIONS) HTML_NO_VIOLATIONS += this.getHTMLHeaderForNoViolations();
        HTML_NO_VIOLATIONS += HTML_FOR_NO_VIOLATIONS;
        return HTML_NO_VIOLATIONS;
    }
}
