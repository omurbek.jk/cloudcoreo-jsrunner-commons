import Constants from '../constants';
import CloudObject from '../models/cloud-object';
import { NumberOrUndefined, StringOrUndefined } from '../interfaces/basic-types';
import SchemeValuesInterface from '../interfaces/scheme-values';

export default class CardHTML {
    card: {
        violations: CloudObject[],
        suppressed: CloudObject[],
    };
    firstCloudObj: CloudObject | undefined;

    constructor() {
        this.card = {
            violations: [],
            suppressed: [],
        };
    }

    private createTextColor(color: string): string {
        return color === Constants.COLORS.WHITE ? Constants.COLORS.GREY : Constants.COLORS.WHITE;
    }

    private getHTMLSuppressedCounterForCard(): string {
        const suppressLength: NumberOrUndefined = this.card.suppressed.length;
        if (!suppressLength) return '';
        return `<span style="background: #d7d7d7; color:#6b6b6b;font-size: 11px;
                        display: inline-block;padding: 5px; border-radius:3px;"
                        >${suppressLength} SUPPRESSED</span>`;
    }

    private getHTMLForSeverityLevel(color: string, textColor: string): string {
        const severityLevel: string = this.firstCloudObj.getSeverityLevel();
        if (!severityLevel) return '';
        return `<div style="padding: 5px;margin-bottom: 10px;font-size: 14px;
                border:1px solid #e4e4e4; text-transform:capitalize;
                display:inline-block;background:${color};font-weight: bold;color:${textColor};">
                ${severityLevel} Severity</div>`;
    }

    private getHTMLForKBLink(): string {
        const kbLink: StringOrUndefined = this.firstCloudObj.getKBLink();
        if (!kbLink) return '';
        return `<span style="float: right;">
                    <a style="font-size: 14px;color:#2B7AE5;text-decoration:none;" 
                    href="${kbLink}"> MORE INFO ›</a></span>`;
    }

    private getHTMLForDisplayName(): string {
        const displayName: StringOrUndefined = this.firstCloudObj.getDisplayName();
        if (!displayName) return '';
        return `<span>${displayName}</span>`;
    }

    private getHTMLForResourceId():string {
        const resourceId: StringOrUndefined = this.firstCloudObj.getResourceName();
        if (!resourceId) return '';
        return `<span style="font-family: Arial; font-size: 12px; 
                color:#000">${resourceId}</span>`;
    }

    private getHTMLCountForCloudObjects(): string {
        const cloudObjectLength: NumberOrUndefined = this.card.violations.length;
        if (cloudObjectLength < 0) return '';
        return `<p style="margin: 5px 0 5px;font-size: 14px;color:#000;"
                >${cloudObjectLength} matching Cloud Objects</p>`;
    }

    private getHTMLForDescription(): string {
        const description: StringOrUndefined = this.firstCloudObj.getDescription();
        if (!description) return '';
        return `<p style="margin:0 0 10px;font-size: 14px;color:#000;">${this.firstCloudObj.getDescription()}</p>`;
    }

    private getHTMLHeaderForCard(): string {
        this.firstCloudObj = this.card.violations[0];
        if (!this.firstCloudObj) return '';
        const color: string = this.firstCloudObj.getColor();
        const htmlForSeverityLevel: string = this.getHTMLForSeverityLevel(color, this.createTextColor(color));
        const htmlForDisplayName: string = this.getHTMLForDisplayName();
        const htmlForKbLink: string = this.getHTMLForKBLink();
        const htmlForResourceId: string = this.getHTMLForResourceId();
        const htmlCountForCloudObjects: string = this.getHTMLCountForCloudObjects();
        const htmlSuppressedCounterForCard: string = this.getHTMLSuppressedCounterForCard();
        const htmlForDescription: string = this.getHTMLForDescription();
        return `
            <div style="font-size: 16px; padding: 0 10px 10px;">
                <div>
                    ${htmlForSeverityLevel}
                </div>
                <div style="font-size: 18px;color:#000;font-weight: bold;">
                    ${htmlForDisplayName}
                    ${htmlForKbLink}
                </div>
                ${htmlForResourceId}
                ${htmlCountForCloudObjects}
                ${htmlSuppressedCounterForCard}
                <div style="height:1px;width:100%; background:#e4e4e4;margin:10px 0 15px;"></div>
                ${htmlForDescription}
            </div>
        `;
    }

    private getColumnWidth(tableKeys: SchemeValuesInterface[] | string[]): number {
        const WIDTH_FOR_ONE_COLUMN: number = Constants.WIDTH_FOR_ONE_COLUMN;
        return tableKeys.length < 1 ? WIDTH_FOR_ONE_COLUMN : WIDTH_FOR_ONE_COLUMN / tableKeys.length;
    }

    private getHTMLTableKeys(): string {
        if (!this.firstCloudObj) return '';
        const tableKeys: string[] = this.firstCloudObj.getTableKeys();
        if (!tableKeys) return '';
        const columnWidth: number = this.getColumnWidth(tableKeys);
        const wrapHTML = '<div style="display:flex;flex-wrap:wrap;">';
        return tableKeys.reduce((html: string, key: string) => {
                return html + this.getHTMLTableKey(columnWidth, key);
            },                  wrapHTML) + '</div>';
    }

    private getHTMLTableKey(columnWidth: number, key: string): string {
        return `<div style="width:calc(${columnWidth}% - 20px);padding: 10px 10px 5px;
font-size: 14px;color:#000;font-weight: bold;">
                ${key}
            </div>`;
    }

    private getHTMLBodyForCard(): string {
        let htmlBodyForCard: string = '<div style="background:#f2f2f2;">';
        htmlBodyForCard += this.getHTMLTableKeys();
        htmlBodyForCard += this.card.violations.reduce((html: string, cloudObj: CloudObject) => {
            return html + this.getHTMLValueFor(cloudObj.getValueForTable());
        },                                             '');
        if (this.card.violations.length && this.card.suppressed.length) {
            htmlBodyForCard += `<div style="background: #d7d7d7;padding-top: 10px;">
                <span style="margin-left:10px;color:#6b6b6b;font-size: 11px;">SUPPRESSED</span>`;
            this.card.suppressed.forEach(cloudObj => {
                htmlBodyForCard += this.getHTMLValueFor(cloudObj.getValueForTable());
            });
            htmlBodyForCard += '</div>';
        }
        return htmlBodyForCard + '</div>';
    }

    private getHTMLValueFor(schemeForTable: SchemeValuesInterface[]): string {
        const schemes: SchemeValuesInterface[] = schemeForTable;
        let htmlValueForTable: string = '<div style="display:flex; flex-wrap:wrap; padding-bottom:5px;">';
        const columnWidth = this.getColumnWidth(schemes);
        schemes.forEach(value => {
            htmlValueForTable += `<div style="word-break:break-all;font-size:14px; color:#000; 
                width:calc(${columnWidth}% - 20px); padding:5px 10px;">`;
            htmlValueForTable += value.hrefForValue ? `<a href="${value.hrefForValue}">${value.schemeValue}</a>` :
                value.schemeValue;
            htmlValueForTable += `</div>`;
        });
        return htmlValueForTable + `</div>`;
    }

    public getHTMLForCard(): string {
        let htmlForCard: string = '<div style="border:1px solid #e4e4e4;margin-bottom:20px;">';
        htmlForCard += this.getHTMLHeaderForCard();
        htmlForCard += this.getHTMLBodyForCard();
        return htmlForCard + '</div>';
    }

    public getHTMLForNoViolations(): string {
        if (this.card.violations.length !== 0) return '';
        const suppressCloudObj: CloudObject | undefined = this.card.suppressed[0];
        if (typeof suppressCloudObj === 'undefined') return '';
        const displayName: StringOrUndefined = suppressCloudObj.getDisplayName();
        const kbLink: StringOrUndefined = suppressCloudObj.getKBLink();
        const htmlSuppressedCounterForCard: string = this.getHTMLSuppressedCounterForCard();
        return `
            <div style="border:1px solid #d6d6d6; border-left:0; border-right:0;padding: 10px 0;
                display: flex;flex-wrap: wrap;align-items: center;
            margin-left: 10px;font-family: Arial, sans-serif;font-weight: bold;">
                <span style="font-size: 18px;color: #000; margin-right: 10px;">
                    ${displayName}  
                </span>
                ${htmlSuppressedCounterForCard}
                <span style="margin-left:auto;">
                    <a style="font-size: 14px;color:#2B7AE5;text-decoration:none;"
                        href="${kbLink}"> MORE INFO ›</a>
                </span>
            </div>`;
    }
}
