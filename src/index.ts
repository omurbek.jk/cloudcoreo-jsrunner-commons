import CardBuilder from './builders/card';
import Email from './models/email';
import SuppressionJSON from './builders/suppression-json';
import CompositeReportInterface from './interfaces/composite-report';
import CompositePropInterface from './interfaces/composite-prop';
import Cards from './models/cards';


export interface argForEmail {
    cards: Cards;
    notificationEmail: string;
    compositeReport: CompositeReportInterface;
    compositeProp: CompositePropInterface;
}

export class CloudCoreoJSRunner {

    public static createEmails(compositeReport: CompositeReportInterface,
                               compositeProp: CompositePropInterface) {
        const cards = CardBuilder.build(compositeReport, compositeProp);
        const noOwnerEmail = compositeProp['NO_OWNER_EMAIL'];
        cards.sortCardsBySeverityLevel();
        const sortedCardsByEmail = cards.getSortedCardsByEmail();
        const hasEmails: boolean = !!sortedCardsByEmail.size;
        const argsForEmail: argForEmail[] = [];
        if (hasEmails) {
            sortedCardsByEmail.forEach((cardsSortedByEmail, notificationEmail) => {
                cardsSortedByEmail.setTableKeysForCards();
                argsForEmail.push({cards: cardsSortedByEmail, notificationEmail, compositeReport, compositeProp});
            });
        } else {
            argsForEmail.push({cards, notificationEmail: noOwnerEmail, compositeReport, compositeProp});
        }

        if (compositeProp.OWNER_TAG !== 'NOT_A_TAG' && noOwnerEmail.length > 0) {
            argsForEmail.push({cards, notificationEmail: noOwnerEmail, compositeReport, compositeProp});
        }

        let emails: Email[] = argsForEmail.map(arg => new Email(arg));

        emails = emails.filter(function (email) {
            if (email.allow_empty === 'true') {
                return email;
            }
            else if (parseInt(email.num_violations, 10) > 0) {
                return email;
            }
        });

        const createSubject = (compositeReport): string => {
            if (compositeReport.htmlReportSubject)
                return `${compositeReport.htmlReportSubject}`;
            else
                return `[${compositeReport.compositeName}] New Detailed Report for ${compositeReport.planName} plan from CloudCoreo`;
        };

        const foundEmail = emails.find((email => email.endpoint.to === noOwnerEmail));
        if (foundEmail) {
            foundEmail.endpoint.subject = createSubject(compositeReport);
        }
        return emails;
    }

    /**
     * Get the JSONWithSuppress
     * @param compositeReport
     * @param compositeProp
     */
    public static createJSONWithSuppress(compositeReport: CompositeReportInterface, compositeProp: CompositePropInterface): CompositeReportInterface {
        return SuppressionJSON.build(compositeReport, compositeProp);
    }
}
